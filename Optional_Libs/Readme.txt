Optional_Libs folder contains librairies that are not part of Mast_Core nor its PDL C/C++ interface.
They can be linked with Mast_Core for custom purposes.
Or they can be used to build applications that exchange with Mast_Core through RPC (Remote Procedure Call)
