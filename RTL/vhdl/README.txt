This directory contains the VHDL files allowing the cosimulation of MAST

NB: This part is still a Work in Progress, and subject to several updates both 
	in terms of source files and documentation

 NB: it is your responsibility to guarantee that the instatiated SUT corresponds   to the SIT file provide to MAST. No check are done. 

The Top-Level Testbench is in ./SUT_Examples/JTAG/SVF_Simulation-top.vhd
 It instantiates a "Master TAP" which communicates with MAST using 
 the two exchange files defined in the "Simulation" Environment Translator 
 (please refer to teh "Mast User Manual"), and well as the System Under Test (SUT). 
  The actual SUT to be instantiated is selected by the glabal variable 
  "target_SUT", defined in ./common/MAST_config.vhd

 To execute a cosimulation under Modelsim/Questasim: 
  - compile MAST as usual and move to this directory
  - execute the script "configure_cosim.sh" to generate symbolic links to the
  	MAST executable and example files
  - check in ./common/MAST_config.vhd for Target_SUT to be set to "TUTORIAL_1"
 svf_simulation_top
  - Execute the script "compile_VHDL.sh" to compile the VHDL files
  - Launch modelsim and load the top level testbench: 
  		vsim work.svf_simulation_top
  - Set Waveform debug as you wish. The "Tutorial_1.do" provides basic signals 
  - edit ./Examples/SIT/JTAG.sit to allow cosimulator by changing the Environment Translator
  	from "TRANSLATOR top Emulation" to "TRANSLATOR top Simulation"
  - launch MAST : ./Mast -c=Cosim.yml -s=/Examples/SIT/JTAG.sit
  	MAST will start, create the exchange files and wait for data from Modelsim
  - launch RTL simulation, for instance "run -all"
  - You will see the usual output from "Console_Incr". 
  -  When Mast has finished execution, you can stop the Modelsim execution and observe the waves
  	NB: In the waveform there are long intervals with no activity: they corresponds to the
	intervals spent by Modelsim to wait for MAST execution.      
	
 EXECUTION TRACES	
    - data_to_rtl.svf contains the SVF commands generated by MAST and sent to Modelsim
    - data_from_rtl.dat contains the binary vectors received from Modelsim
    - ./Simulation_Exchange_Files/tutorial_1/ contains log files for the SUT register:
    	- reg.out logs all values updated to the register. They should correspond the the iWrites in the PDL code
	- reg.in contains the values that are captured to the register. They should correspond the the iGet 
		in the PDL code
		
 
   GOING FORWARD
  - SVF vectors generated offline, or by a previous execution of MAST, can be simulated in Modelsim
  	by pre-filling the data_to_rtl.svf file and simply launching the simulation. 
  - the current repository contains also a VHDL implementation of the MIB_Example example,called 
  	MIB_Tutorial in VHDL.	
