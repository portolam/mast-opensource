[Chain]        "Nested_SIB_3WI"
 [Chain]        "SIB1", ignore_in_path: true
  [Linker]       "SIB1_mux"
   :Selector:     "SIB1_ctrl", kind: Binary, can_select_none: true, inverted_bits: false, reversed_order: false
   Selection Table:
     [0] 0b0
     [1] 0b1
   Deselection Table:
     [0] 0b0
     [1] 0b0
   [Chain]        "SIB1_1", ignore_in_path: true
    [Chain]        "WI1"
     [Chain]        "WrappedInstr"
      [Chain]        "reg8"
       [Chain]        "SReg"
        [Register]     "SR", length: 8, bypass: 0000_0000
    [Chain]        "SIB2", ignore_in_path: true
     [Linker]       "SIB2_mux"
      :Selector:     "SIB2_ctrl", kind: Binary, can_select_none: true, inverted_bits: false, reversed_order: false
      Selection Table:
        [0] 0b0
        [1] 0b1
      Deselection Table:
        [0] 0b0
        [1] 0b0
      [Chain]        "SIB2_1", ignore_in_path: true
       [Chain]        "WI2"
        [Chain]        "WrappedInstr"
         [Chain]        "reg8"
          [Chain]        "SReg"
           [Register]     "SR", length: 8, bypass: 0000_0000
       [Chain]        "SIB3", ignore_in_path: true
        [Linker]       "SIB3_mux"
         :Selector:     "SIB3_ctrl", kind: Binary, can_select_none: true, inverted_bits: false, reversed_order: false
         Selection Table:
           [0] 0b0
           [1] 0b1
         Deselection Table:
           [0] 0b0
           [1] 0b0
         [Chain]        "WI3"
          [Chain]        "WrappedInstr"
           [Chain]        "reg8"
            [Chain]        "SReg"
             [Register]     "SR", length: 8, bypass: 0000_0000
        [Register]     "SIB3_ctrl", length: 1, Hold value: true, bypass: 0
     [Register]     "SIB2_ctrl", length: 1, Hold value: true, bypass: 0
  [Register]     "SIB1_ctrl", length: 1, Hold value: true, bypass: 0
