// this is a valid in-line line comment
Module Demo { // this is a valid line comment
/* this is a
valid block comment */
/* this is /* not */ a valid comment
because it contains a nested comment */
}
