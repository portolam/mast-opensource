#------------------------------------------------------------------------------#
# This makefile was generated by 'cbp2make' tool rev.147                       #
#------------------------------------------------------------------------------#

.RECIPEPREFIX = >

WORKDIR = `pwd`

CC = gcc-4.9
CXX = g++-4.9
AR = ar-4.9
LD = g++-4.9
WINDRES = windres

INC =
WARNINGS = -Wall -Wpedantic  -Wnon-virtual-dtor -Wredundant-decls -Wundef -Wmissing-include-dirs -Wswitch-enum -Wswitch-default
CFLAGS = $(WARNINGS) -std=c++14 -fPIC
RESINC =
LIBDIR =
LIB = -lpthread
LDFLAGS =

INC_DEBUG     = $(INC)
CFLAGS_DEBUG  = $(CFLAGS) -g -DBUILD_DLL
RESINC_DEBUG  = $(RESINC)
RCFLAGS_DEBUG = $(RCFLAGS)
LIBDIR_DEBUG  = $(LIBDIR)
LIB_DEBUG     = $(LIB)
LDFLAGS_DEBUG = $(LDFLAGS) -Wl,--dll
OBJDIR_DEBUG  = ../obj/debug
DEP_DEBUG     =
OUT_DEBUG     = ../bin/debug/Logger.so

INC_RELEASE     = $(INC)
CFLAGS_RELEASE  = $(CFLAGS) -O2 -DBUILD_DLL
RESINC_RELEASE  = $(RESINC)
RCFLAGS_RELEASE = $(RCFLAGS)
LIBDIR_RELEASE  = $(LIBDIR)
LIB_RELEASE     = $(LIB)
LDFLAGS_RELEASE = $(LDFLAGS) -s -Wl,--dll
OBJDIR_RELEASE  = ../obj/release
DEP_RELEASE     =
OUT_RELEASE     = ../bin/release/Logger.so

OBJ_DEBUG = \
            $(OBJDIR_DEBUG)/crashhandler_unix.o    \
            $(OBJDIR_DEBUG)/LogFormatter.o         \
            $(OBJDIR_DEBUG)/LoggerSinks.o          \
            $(OBJDIR_DEBUG)/CustomFileSink.o       \
            $(OBJDIR_DEBUG)/filesink.o             \
            $(OBJDIR_DEBUG)/g3log.o                \
            $(OBJDIR_DEBUG)/logcapture.o           \
            $(OBJDIR_DEBUG)/loglevels.o            \
            $(OBJDIR_DEBUG)/logmessage.o           \
            $(OBJDIR_DEBUG)/logworker.o            \
            $(OBJDIR_DEBUG)/time.o

OBJ_RELEASE = \
            $(OBJDIR_RELEASE)/crashhandler_unix.o    \
            $(OBJDIR_RELEASE)/LogFormatter.o         \
            $(OBJDIR_RELEASE)/LoggerSinks.o          \
            $(OBJDIR_RELEASE)/CustomFileSink.o       \
            $(OBJDIR_RELEASE)/filesink.o             \
            $(OBJDIR_RELEASE)/g3log.o                \
            $(OBJDIR_RELEASE)/logcapture.o           \
            $(OBJDIR_RELEASE)/loglevels.o            \
            $(OBJDIR_RELEASE)/logmessage.o           \
            $(OBJDIR_RELEASE)/logworker.o            \
            $(OBJDIR_RELEASE)/time.o

all: debug release

clean: clean_debug clean_release

before_debug:
> test -d ../bin/debug    || mkdir -p ../bin/debug
> test -d $(OBJDIR_DEBUG) || mkdir -p $(OBJDIR_DEBUG)

after_debug:

debug: before_debug out_debug after_debug

out_debug: before_debug $(OBJ_DEBUG) $(DEP_DEBUG)
> $(LD) -shared $(LIBDIR_DEBUG) $(OBJ_DEBUG)  -o $(OUT_DEBUG) $(LDFLAGS_DEBUG) $(LIB_DEBUG)

$(OBJDIR_DEBUG)/crashhandler_unix.o: Unix/crashhandler_unix.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c Unix/crashhandler_unix.cpp -o $(OBJDIR_DEBUG)/crashhandler_unix.o

$(OBJDIR_DEBUG)/CustomFileSink.o: CustomFileSink.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c CustomFileSink.cpp -o $(OBJDIR_DEBUG)/CustomFileSink.o


$(OBJDIR_DEBUG)/g3log.o: g3log.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c g3log.cpp -o $(OBJDIR_DEBUG)/g3log.o

$(OBJDIR_DEBUG)/filesink.o: filesink.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c filesink.cpp -o $(OBJDIR_DEBUG)/filesink.o

$(OBJDIR_DEBUG)/logcapture.o: logcapture.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c logcapture.cpp -o $(OBJDIR_DEBUG)/logcapture.o

$(OBJDIR_DEBUG)/LogFormatter.o: LogFormatter.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c LogFormatter.cpp -o $(OBJDIR_DEBUG)/LogFormatter.o

$(OBJDIR_DEBUG)/LoggerSinks.o: LoggerSinks.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c LoggerSinks.cpp -o $(OBJDIR_DEBUG)/LoggerSinks.o

$(OBJDIR_DEBUG)/loglevels.o: loglevels.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c loglevels.cpp -o $(OBJDIR_DEBUG)/loglevels.o

$(OBJDIR_DEBUG)/logmessage.o: logmessage.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c logmessage.cpp -o $(OBJDIR_DEBUG)/logmessage.o

$(OBJDIR_DEBUG)/logworker.o: logworker.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c logworker.cpp -o $(OBJDIR_DEBUG)/logworker.o

$(OBJDIR_DEBUG)/time.o: time.cpp
> $(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c time.cpp -o $(OBJDIR_DEBUG)/time.o


clean_debug:
> rm -f $(OBJ_DEBUG) $(OUT_DEBUG)
> rm -rf ../bin/debug
> rm -rf $(OBJDIR_DEBUG)

before_release:
> test -d ../bin/release    || mkdir -p ../bin/release
> test -d $(OBJDIR_RELEASE) || mkdir -p $(OBJDIR_RELEASE)

after_release:

release: before_release out_release after_release

out_release: before_release $(OBJ_RELEASE) $(DEP_RELEASE)
> $(LD) -shared $(LIBDIR_RELEASE) $(OBJ_RELEASE)  -o $(OUT_RELEASE) $(LDFLAGS_RELEASE) $(LIB_RELEASE)

$(OBJDIR_RELEASE)/crashhandler_unix.o: Unix/crashhandler_unix.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c Unix/crashhandler_unix.cpp -o $(OBJDIR_RELEASE)/crashhandler_unix.o

$(OBJDIR_RELEASE)/CustomFileSink.o: CustomFileSink.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c CustomFileSink.cpp -o $(OBJDIR_RELEASE)/CustomFileSink.o

$(OBJDIR_RELEASE)/g3log.o: g3log.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c g3log.cpp -o $(OBJDIR_RELEASE)/g3log.o

$(OBJDIR_RELEASE)/filesink.o: filesink.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c filesink.cpp -o $(OBJDIR_RELEASE)/filesink.o

$(OBJDIR_RELEASE)/logcapture.o: logcapture.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c logcapture.cpp -o $(OBJDIR_RELEASE)/logcapture.o

$(OBJDIR_RELEASE)/LogFormatter.o: LogFormatter.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c LogFormatter.cpp -o $(OBJDIR_RELEASE)/LogFormatter.o

$(OBJDIR_RELEASE)/LoggerSinks.o: LoggerSinks.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c LoggerSinks.cpp -o $(OBJDIR_RELEASE)/LoggerSinks.o

$(OBJDIR_RELEASE)/loglevels.o: loglevels.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c loglevels.cpp -o $(OBJDIR_RELEASE)/loglevels.o

$(OBJDIR_RELEASE)/logmessage.o: logmessage.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c logmessage.cpp -o $(OBJDIR_RELEASE)/logmessage.o

$(OBJDIR_RELEASE)/logworker.o: logworker.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c logworker.cpp -o $(OBJDIR_RELEASE)/logworker.o

$(OBJDIR_RELEASE)/time.o: time.cpp
> $(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c time.cpp -o $(OBJDIR_RELEASE)/time.o


clean_release:
> rm -f $(OBJ_RELEASE) $(OUT_RELEASE)
> rm -rf ../bin/release
> rm -rf $(OBJDIR_RELEASE)

.PHONY: before_debug after_debug clean_debug before_release after_release clean_release

