project(Mast_API_CPP)

include_directories(include)
include_directories(internal)
include_directories(${LIBMAST_CORE_INTERNAL_DIRS})
include_directories(${LIBTCLAP_INCLUDE_DIRS})
include_directories(${LIBLOGGER_INCLUDE_DIRS})

set(LIBMAST_CPP_API_INCLUDE_DIRS  "${CMAKE_CURRENT_SOURCE_DIR}/include"  PARENT_SCOPE)
set(LIBMAST_CPP_API_INTERNAL_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/internal" PARENT_SCOPE)

set (CFLAGS "${CFLAGS} -DG3_DYNAMIC_LOGGING")
set (CFLAGS "${CFLAGS} -DCPP_API_EXPORTS")

if (CODE_COVERAGE)
  set(CMAKE_CXX_FLAGS "--coverage ${CMAKE_CXX_FLAGS}")
endif()

file(GLOB Mast_SOURCES_CPP_API "src/*")

set(Mast_SOURCES_CPP_API_PDL "${CMAKE_CURRENT_LIST_DIR}/src/PDL_Adapter_CPP.cpp"
)

set (Mast_SOURCES ${Mast_SOURCES_CPP_API})

add_library           (Mast_API_CPP SHARED   ${Mast_SOURCES})
set_target_properties (Mast_API_CPP PROPERTIES COMPILE_FLAGS "${CFLAGS}")

add_library           (Mast_API_CPP_PDL SHARED   ${Mast_SOURCES_CPP_API_PDL})
set_target_properties (Mast_API_CPP_PDL PROPERTIES COMPILE_FLAGS "${CFLAGS}")


if (WIN32)
  set(EXTRA_LIBS ${EXTRA_LIBS} DbgHelp)
else ()
  set(EXTRA_LIBS ${EXTRA_LIBS} pthread dl)
endif ()

message("")
message("================================================================================")
message(STATUS "Mast_API_CPP: CODE_COVERAGE           ${CODE_COVERAGE}")
message(STATUS "Mast_API_CPP: LIBLOGGER_INCLUDE_DIRS: ${LIBLOGGER_INCLUDE_DIRS}")
message(STATUS "Mast_API_CPP: CFLAGS:                 ${CFLAGS}")
message(STATUS "Mast_API_CPP: EXTRA_LIBS:             ${EXTRA_LIBS}")
message(STATUS "Mast_API_CPP: CMAKE_CXX_FLAGS:        ${CMAKE_CXX_FLAGS}")
message("================================================================================")
message("")

target_link_libraries(Mast_API_CPP Mast_Core SIT_Reader ICL_Reader Logger ${EXTRA_LIBS})
target_link_libraries(Mast_API_CPP_PDL Mast_Core Logger ${EXTRA_LIBS})
target_include_directories(Mast_API_CPP PUBLIC include)
