//===========================================================================
//                           CPP_API.hpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file CPP_API.hpp
//!
//! Includes all files needed to use MAST through its 'C++' language API
//!
//===========================================================================


#ifndef CPP_API_H__FFF980B7_80C0_4B66_1498_1F0C27A635AD__INCLUDED_
  #define CPP_API_H__FFF980B7_80C0_4B66_1498_1F0C27A635AD__INCLUDED_

#include "AppFunctionAndName.hpp"
#include "AppFunctionAndNodePath.hpp"
#include "PDL_Adapter_CPP.hpp"
#include "SystemModelAdapter_CPP.hpp"

#endif  // not defined CPP_API_H__FFF980B7_80C0_4B66_1498_1F0C27A635AD__INCLUDED_
//===========================================================================
// End of CPP_API.hpp
//===========================================================================



