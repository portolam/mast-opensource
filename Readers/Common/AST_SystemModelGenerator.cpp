//===========================================================================
//                           AST_SystemModelGenerator.cpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file AST_SystemModelGenerator.cpp
//!
//! Implements class AST_SystemModelGenerator
//!
//===========================================================================

#include "AST_SystemModelGenerator.hpp"

#include "AST_AccessLink.hpp"
#include "AST_Alias.hpp"
#include "AST_AliasConverter.hpp"
#include "AST_Attribute.hpp"
#include "AST_AttributesConverter.hpp"
#include "AST_BsdlInstructionRef.hpp"
#include "AST_ConcatNumber.hpp"
#include "AST_FileRef.hpp"
#include "AST_Instance.hpp"
#include "AST_Module.hpp"
#include "AST_ModuleIdentifier.hpp"
#include "AST_Namespace.hpp"
#include "AST_Network.hpp"
#include "AST_Parameter.hpp"
#include "AST_ParameterRef.hpp"
#include "AST_Port.hpp"
#include "AST_ScalarIdentifier.hpp"
#include "AST_ScanInterface.hpp"
#include "AST_ScanInterfaceRef.hpp"
#include "AST_ScanMux.hpp"
#include "AST_ScanMuxSelection.hpp"
#include "AST_ScanRegister.hpp"
#include "AST_Signal.hpp"
#include "AST_Source.hpp"
#include "AST_String.hpp"
#include "AST_VectorIdentifier.hpp"
#include "AST_Helper.hpp"

#include "AccessInterfaceProtocol.hpp"
#include "AccessInterfaceProtocolFactory.hpp"
#include "DefaultTableBasedPathSelector.hpp"
#include "UnresolvedPathSelector.hpp"
#include "RegistersAlias.hpp"
#include "SystemModel.hpp"
#include "SystemModelBuilder.hpp"
#include "BSDL_Reader.hpp"
#include "Utility.hpp"
#include "MastConfig.hpp"
#include "g3log/g3log.hpp"

#include <tuple>
#include <functional>
#include <experimental/string_view>

using std::stack;
using std::queue;
using std::vector;
using std::shared_ptr;
using std::unique_ptr;
using std::tuple;
using std::reference_wrapper;
using std::string;
using std::experimental::string_view;
using std::make_unique;
using std::make_shared;
using std::make_tuple;

using namespace mast;
using namespace Parsers;

const std::vector<AST_Signal*> AST_SystemModelGenerator::sm_noSignals;    // This is to report end of scan path !

//! Destructs AST_SystemModelGenerator
//!
AST_SystemModelGenerator::~AST_SystemModelGenerator ()
{
}
//
//  End of: AST_SystemModelGenerator::~AST_SystemModelGenerator
//---------------------------------------------------------------------------


//! Constructor...
AST_SystemModelGenerator::AST_SystemModelGenerator (shared_ptr<mast::SystemModel> systemModel)
  : m_systemModel (CHECK_PARAMETER_NOT_NULL(systemModel, "Expect valid, not nullptr SystemModel"))
  , m_builder     (make_unique<SystemModelBuilder>(*m_systemModel))
{
}
//
//  End of: AST_SystemModelGenerator::AST_SystemModelGenerator
//---------------------------------------------------------------------------



//! Appends created children to a parent node
//!
//! @param parent           Parent to assign childre to
//! @param levelThreshold   Minimal count of created children to leave unappended
//!
void AST_SystemModelGenerator::AppendCreatedNodesToParent (ParentNode* parent, size_t levelThreshold)
{
  while (m_createdNodes.size() > levelThreshold)
  {
    auto child = m_createdNodes.back().node;

    parent->AppendChild(child);
    m_createdNodes.pop_back();
  }
}
//
//  End of: AST_SystemModelGenerator::AppendCreatedNodesToParent
//---------------------------------------------------------------------------



//! Assigns newly created node to its parent unless it is a linker or representing top module.
//! In the latter case, it is pushed into a stack of not assigned yet nodes.
//!
//! @param node A newly created node
//!
void AST_SystemModelGenerator::AssignNewNode (shared_ptr<SystemModelNode> node)
{
  auto& context           = m_instancesContext.top();
  auto  parentNode        = context.parentNode;
  auto  linkerNode        = context.linkerNode;
  auto  parentMayBeLinker = linkerNode != nullptr;

  auto contextIsTopModule = m_instancesContext.size() == 1u;

  if (parentMayBeLinker || contextIsTopModule)
  {
    m_createdNodes.push_back({node, parentNode});
  }
  else
  {
    parentNode->PrependChild(node);
  }
}
//
//  End of: AST_SystemModelGenerator::AssignNewNode
//---------------------------------------------------------------------------


//! Appends not yet assigned node to Linker first selection, provided there is some
//! to assign to Linker selection
//!
//! @internal
//! @note   Supposing that assigned node stack is filled up like (last one is nearest from scan input):
//!           [1] [L] [3] [4] [C] [6] [7] [8]
//!                ^
//!         where:
//!           - brackets pair represents a node,
//!           - [L] is the node for the Linker
//!           - [C] is the node (most often a Chain) that was reached following all Linker selection paths
//!           - For SIB, [L] & [C] are next to each other ==> First Linker selection is empty ==> [C] and the following are sibling of the Linker
//!
//!         The algorithm purpose is to extract the nodes between [L] and [C]:
//!           1 - extracts 2nd part into a temporary stack          ==> [8] [7] [6] [C]
//!           2 - extracts 1st part into another temporary stack    ==> [4] [3]
//!           3 - Append 1st part Linker 1st selection              ==> [4] [3] - note that they are assign in that order - (inserting a Chain in between in that case)
//!           4 - Restore 2nd part to not yet assigned nodes stack  ==> [1] [L] [C] [6] [7] [8]
//!               or assign previous siblings to common, parent     ==> e.g. Parent children: [8]->[7]->[6]->[C]
//!               ==> Candidates must be reversed before being able to "splice" them
//!               ==> They must also been inserted BEFORE nodes assigned to parent when traversing connectivity backward
//!               ==> They must also been inserted AFTER nodes assigned when dealing with Linker 1st selection (traversing connectivity forward)
//! @endinternal
//!
//! @return True when first Linker selection is empty, false otherwise
bool AST_SystemModelGenerator::AssignNodesToLinkerFirstSelection ()
{
  auto firstSelectionIsEmpty = true;

  auto& linkerContext          = m_linkersContext.back();
  auto  linker                 = linkerContext.linker;
  auto  nodesLevelAfterCreate  = linkerContext.nodesLevelAfterCreate;
  auto  nodesLevelAfter1stPath = linkerContext.nodesLevelAfter1stPath;
  auto  commonLinkerNode       = linkerContext.commonLinkerNode;

  CHECK_VALUE_GTE(m_createdNodes.size(), nodesLevelAfterCreate, "Have appended more nodes than expected (for 1st Linker selection)");
  auto maxCreateForFirst = nodesLevelAfter1stPath - nodesLevelAfterCreate;

  // Adjust real child count for 1st selection (dealing with common node on paths)
  //
  if (maxCreateForFirst == 0)
  {
    return firstSelectionIsEmpty;
  }

  // 2nd part extraction
  stack<CreatedNodes>  nodesAsLinkerSiblings;
  auto createdAfterFirst = m_createdNodes.size() - nodesLevelAfter1stPath;
  CHECK_VALUE_EQ(createdAfterFirst, 0, "Houps: Some created nodes have not being assigned to Linker derivations (after 1st)");

  auto foundCommonNode =    (commonLinkerNode != nullptr)
                            && (std::find_if(m_createdNodes.cbegin(),
                                             m_createdNodes.cend(),
                                             [commonLinkerNode](const auto& item) { return item.node.get() == commonLinkerNode; })
                                != m_createdNodes.cend());

  // Extracts linker previous siblings
  //
  if (foundCommonNode)
  {
    while (m_createdNodes.size() > nodesLevelAfterCreate)
    {
      auto nodeAndParent = m_createdNodes.back();
      nodesAsLinkerSiblings.push(nodeAndParent);
      m_createdNodes.pop_back();

      if (nodeAndParent.node.get() == commonLinkerNode)
      {
        break;
      }
    }
  }

  // 1st part extraction (nodes for 1st selection)
  stack<shared_ptr<SystemModelNode>>  nodesForFirstSelection;
  while (m_createdNodes.size() > nodesLevelAfterCreate)
  {
    auto node = m_createdNodes.back().node;  // Get rid of "alternate" parent node (this is in fact resolved as the Linker)
    if (node.get() == commonLinkerNode)
    {
      nodesAsLinkerSiblings.push(m_createdNodes.back());
    }
    else
    {
      nodesForFirstSelection.push(node);
    }
    m_createdNodes.pop_back();
  }

  // Append 1st part Linker 1st selection (inserting a Chain when there is more than a single node)
  auto createdForFirst = nodesForFirstSelection.size();
  if (createdForFirst != 0)
  {
    firstSelectionIsEmpty = false;
    if (createdForFirst == 1)
    {
      linker->PrependChild(nodesForFirstSelection.top());
    }
    else
    {
      auto chain = Create_ChainForLinker(linker, 0);
      linker->PrependChild(chain);
      while (!nodesForFirstSelection.empty())
      {
        chain->PrependChild(nodesForFirstSelection.top());
        nodesForFirstSelection.pop();
      }
    }
  }

  // Restore 2nd part to not yet assigned nodes stack
  // or prepend or save for splicing them as linker, previous, siblings (sharing same parent node)
  stack<CreatedNodes>  nodesToSpliceAsLinkerPreviousSiblings;
  auto linkerParentNode = linkerContext.linkerParentNode;
  auto processingSibblings = true;
  while (!nodesAsLinkerSiblings.empty())
  {
    auto& nodeAndParent = nodesAsLinkerSiblings.top();

    auto node       = nodeAndParent.node;
    auto parentNode = nodeAndParent.parentNode;
    CHECK_VALUE_NOT_NULL(parentNode, "Parent node must always be set in m_createdNodes stack");

    if (processingSibblings && (parentNode == linkerParentNode))  // Linker sibling with same parent node must be contiguous
    {
      auto hasAlreadySplicePoint = m_parentsSplicePoint.find(parentNode) != m_parentsSplicePoint.end();
      if (hasAlreadySplicePoint)
      {
        nodesToSpliceAsLinkerPreviousSiblings.push(nodeAndParent);
      }
      else
      {
        parentNode->PrependChild(node);                        // Create splice point (can only splice after some node)
        m_parentsSplicePoint.insert({parentNode, node.get()}); // Save splice point
      }
    }
    else
    {
      processingSibblings = false;   // From now on, there will be no more linker (previous) siblings
      m_createdNodes.push_back(nodeAndParent);
    }
    nodesAsLinkerSiblings.pop();
  }

  // Splice, remaining, linker siblings
  while (!nodesToSpliceAsLinkerPreviousSiblings.empty())
  {
    auto& nodeAndParent = nodesToSpliceAsLinkerPreviousSiblings.top();

    auto spliceNode = m_parentsSplicePoint[nodeAndParent.parentNode];
    CHECK_PARAMETER_NOT_NULL(spliceNode, "Houps, there should be a valid splice point node");

    spliceNode->SpliceSibling(nodeAndParent.node);
    nodesToSpliceAsLinkerPreviousSiblings.pop();
  }
  return firstSelectionIsEmpty;
}
//
//  End of: AST_SystemModelGenerator::AssignNodesToLinkerFirstSelection
//---------------------------------------------------------------------------



//! Counts number of bits that will drive a ScanMux
//!
//! @param selectorRegisters  Info about each ScanRegisters used to drive the ScanMux
//!
//! @return The total number of bits used from selector registers
uint32_t AST_SystemModelGenerator::CountSelectorRegistersBitsCount (const vector<SelectorRegistersInfo>& selectorRegisters)
{
  uint32_t bitsCount = 0;

  for (const auto& info : selectorRegisters)
  {
    if (info.hasRange)
    {
      auto rangeWidth = IndexedRange(info.leftIndex, info.rightIndex).Width();

      bitsCount += rangeWidth;
    }
    else
    {
      bitsCount += info.scanRegister->BitsCount();
    }
  }

  return bitsCount;
}
//
//  End of: AST_SystemModelGenerator::CountSelectorRegistersBitsCount
//---------------------------------------------------------------------------



//! Creates a "generated" chain to force a single child for Linker selection
//!
//! @param linker         Linker to create a chain for
//! @param selectionId    Selection id for which the Chain is created
//!
shared_ptr<mast::Chain> AST_SystemModelGenerator::Create_ChainForLinker (const Linker* linker, size_t selectionId)
{
  auto name  = linker->Name();
  name.append("_").append(std::to_string(selectionId));
  auto chain = m_systemModel->CreateChain(name);
  chain->IgnoreForNodePath(true);
  return chain;
}
//
//  End of: AST_SystemModelGenerator::Create_ChainForLinker
//---------------------------------------------------------------------------


//! Creates a path selector for linker
//!
//! @note ScanRegister must have already been converted to SystemModel Register
//!
//! @param selectorRegistersInfos   Info about ScanMux paths selector
//! @param selectTable              Select table
//! @param deselectTable            Deselect table
//!
//! @return Created path selector
shared_ptr<PathSelector> AST_SystemModelGenerator::Create_PathSelector (const vector<SelectorRegistersInfo>& selectorRegistersInfos,
                                                                        vector<BinaryVector>&&               selectTable,
                                                                        vector<BinaryVector>&&               deselectTable,
                                                                        SelectorProperty                     selectorProperties)
{
  VirtualRegister virtualRegister;

  for (const auto& selectorItem : selectorRegistersInfos)
  {
    auto scanRegister  = selectorItem.scanRegister;
    auto modelRegister = scanRegister->AssociatedRegister();

    CHECK_VALUE_NOT_NULL(modelRegister, "ScanRegister \""s + scanRegister->Name() + "\" has been found outside any scan chain ==> this is not yet supported");

    modelRegister->SetHoldValue(true);

    auto range = selectorItem.hasRange ? IndexedRange{selectorItem.leftIndex, selectorItem.rightIndex}
                                       : IndexedRange{modelRegister->BitsCount() - 1u, 0u};

    RegisterSlice registerSlice{modelRegister, range};

    virtualRegister.Append(registerSlice);
  }

  auto pathsCount   = selectTable.size() - 1u;
  auto pathSelector = make_shared<DefaultTableBasedPathSelector>(virtualRegister,
                                                                 pathsCount,
                                                                 std::move(selectTable),
                                                                 std::move(deselectTable),
                                                                 selectorProperties);
  return pathSelector;
}
//
//  End of: AST_SystemModelGenerator::Create_PathSelector
//---------------------------------------------------------------------------


//! Creates linker path selector
//!
//! @param scanMux                ScanMux representing the linker
//! @param module                 Module the in which the ScanMux is defined
//! @param firstSelectionIsEmpty  When true, first mux selection is ignored in selection/deselection tables (and the linker must be set with can_select_none = true)
//!
shared_ptr<PathSelector> AST_SystemModelGenerator::Create_PathSelector (AST_ScanMux* scanMux, AST_Module* module, bool firstSelectionIsEmpty)
{
  // ---------------- Collect selector(s) path(s) including bits ranges
  // Paths are ordered and defined using SystemModel path syntax
  // Bits ranges are defined as pair of integers
  //
  const auto& selectors = scanMux->Selectors();

  const auto selectorRegisters  = FindSelectorRegisters(selectors, module);
  const auto selectorsBitsCount = CountSelectorRegistersBitsCount(selectorRegisters);

  // ---------------- Prepare selection/deselection tables
  //
  vector<BinaryVector> selectTable;
  vector<BinaryVector> deselectTable;

  auto const& selections = scanMux->Selections();
  std::tie(selectTable, deselectTable) = MakeSelectionTable(selections, selectorsBitsCount, firstSelectionIsEmpty);

  // ---------------- Prepare path selector
  //
  auto selectorProperties = firstSelectionIsEmpty ? SelectorProperty::CanSelectNone
                                                  : SelectorProperty::Std;

  // ---------------- Check that all ScanRegisters have been tranformed to SystemModel Register
  //
  auto hasNotYetConvertedSelector = false;
  for (const auto& selectorItem : selectorRegisters)
  {
    if (selectorItem.scanRegister->AssociatedRegister() == nullptr)
    {
      hasNotYetConvertedSelector = true;
      break;
    }
  }

  // ---------------- Create path selector (possibly unresolved yet)
  //
  if (hasNotYetConvertedSelector)
  {
    auto unresolvedPathSelector = make_shared<UnresolvedPathSelector>();
    unresolvedPathSelector->SelectionTables(std::move(selectTable), std::move(deselectTable));
    unresolvedPathSelector->Properties(selectorProperties);

    UnresolvedPathSelectorInfo unresolvedInfo{scanMux, std::move(selectorRegisters), unresolvedPathSelector};

    m_unresolvedPathSelectorsInfos.emplace_back(unresolvedInfo);

    return unresolvedPathSelector;
  }

  auto pathSelector = Create_PathSelector(selectorRegisters,
                                          std::move(selectTable),
                                          std::move(deselectTable),
                                          selectorProperties);

  return pathSelector;
}
//
//  End of: AST_SystemModelGenerator::Create_PathSelector
//---------------------------------------------------------------------------



//! Creates a AccessInterfaceProtocol for top module TAP
//!
//! @param topModule  Network top module with JTAG TAP kind AccessLink
//!
std::unique_ptr<AccessInterfaceProtocol> AST_SystemModelGenerator::Create_Protocol (AST_Module* topModule)
{
  if (m_protocolName.empty())
  {
    const auto protocolNameAttribute       = topModule->Attribute("ACCESS_LINK_PROTOCOL_NAME");
    const auto protocolParametersAttribute = topModule->Attribute("ACCESS_LINK_PROTOCOL_PARAMETERS");

    CHECK_PARAMETER_NOT_NULL(protocolNameAttribute, "Missing protocol name for tap: \""s.append(topModule->AccessLink()->Name()).append("\""));

    m_protocolName = protocolNameAttribute->ValueAsText();
    if (protocolParametersAttribute != nullptr)
    {
      m_protocolParameters = protocolParametersAttribute->ValueAsText();
    }
  }

  LOG(INFO) << "Using JTAG TAP with protocol: " << m_protocolName << " and parameters: " << m_protocolParameters;

  auto& factory  = AccessInterfaceProtocolFactory::Instance();
  auto  protocol = factory.Create(m_protocolName, m_protocolParameters);

  CHECK_VALUE_NOT_NULL(protocol.get(), "Failed to create a protocol with name \""s.append(m_protocolName).append("\""));
  return protocol;
}
//
//  End of: AST_SystemModelGenerator::Create_Protocol
//---------------------------------------------------------------------------



//! Finds ScanInterface defined by ScanInterface reference in a module
//!
//! @param module             Module from which search operates
//! @param instanceRef        Optional Instance reference (when the ScanInterface is defined in an Instance module)
//! @param scanInterfaceName  Name of the ScanInterface to look for
//!
tuple<AST_Instance*, AST_Module*, AST_ScanInterface*>
AST_SystemModelGenerator::FindScanInterface (AST_Module*           module,
                                             AST_ScalarIdentifier* instanceRef,
                                             const string&         scanInterfaceName)
{
  if (instanceRef != nullptr)
  {
    auto instance         = module->FindInstance(instanceRef);
    CHECK_VALUE_NOT_NULL(instance, "Cannot find instance \""s.append(instanceRef->AsText()).append("\""));

    auto instanceModule = instance->UniquifiedModule();
    CHECK_VALUE_NOT_NULL(instanceModule, "Cannot find module instance \""s.append(instanceRef->AsText()).append("\""));

    auto scanInterface = instanceModule->FindScanInterface(scanInterfaceName);
    CHECK_VALUE_NOT_NULL(scanInterface, "Cannot find ScanInterface \""s.append(scanInterfaceName)
                                        .append("\" in instance \"").append(instanceRef->AsText()).append("\""));

    return make_tuple(instance, instanceModule, scanInterface);
  }

  // ==> Scan interface must be in module definition
  auto scanInterface = module->FindScanInterface(scanInterfaceName);

  CHECK_VALUE_NOT_NULL(scanInterface, "Cannot find ScanInterface \""s.append(scanInterfaceName)
                                      .append("\" in module\"").append(module->Name()).append("\""));

  return make_tuple(nullptr, module, scanInterface);
}
//
//  End of: AST_SystemModelGenerator::FindScanInterface
//---------------------------------------------------------------------------



//! Finds a ScanOutPort defined in a ScanInterface
//!
//! @param module         Module in which the ScanInterface is defined
//! @param scanInterface  ScanInterface defining ScanOutPort
//!
const AST_Port* AST_SystemModelGenerator::FindScanOutPort (AST_Module* module, const AST_ScanInterface* scanInterface)
{
  const auto& ports = scanInterface->Ports();

  for (const auto& port : ports)
  {
    const auto modulePort = module->FindScanOutPort(port->Identifier());
    if (modulePort != nullptr)
    {
      return modulePort;
    }
  }

  CHECK_FAILED("Failed to find ScanOutPort defined by ScanInterface \""s.append(scanInterface->Name())
               .append("\" in module \"").append(module->Name()).append("\""));
}
//
//  End of: AST_SystemModelGenerator::FindScanOutPort
//---------------------------------------------------------------------------


//! Follows ScanMux selector signals to find driving ScanRegister(s)
//!
//! @param selectors  ScanMux selector signals
//! @param module     ScanMux parent module
//!
//! @return Ordered Sets of driving ScanRegisters along with whether it has bits span (left and right)
//!
vector<AST_SystemModelGenerator::SelectorRegistersInfo>
AST_SystemModelGenerator::FindSelectorRegisters (const std::vector<Parsers::AST_Signal*>& selectors,
                                                 AST_Module*                              module)
{
  vector<SelectorRegistersInfo> selectorRegistersInfos;

  // ---------------- Follow each selector signal
  //
  for (const auto& selector : selectors)
  {
    auto scanRegister = AST_Helper::FollowSignalTilScanRegister(module, selector);

    CHECK_VALUE_NOT_NULL(scanRegister, "Failed to find ScanRegister selector for scan_mux in module \""s.append(module->Name()).append("\""));

    bool     hasRange   = false;
    uint32_t leftIndex  = 0u;
    uint32_t rightIndex = 0u;

    const auto identifier = selector->PortName();
    if (identifier->IsKind(Kind::VectorIdentifier))
    {
      auto asVectorIdentifier = static_cast<const AST_VectorIdentifier*>(identifier);

      std::tie(hasRange, leftIndex, rightIndex) = asVectorIdentifier->Range();
    }

    SelectorRegistersInfo info{ scanRegister, hasRange, leftIndex, rightIndex };
    selectorRegistersInfos.emplace_back(info);
  }

  return selectorRegistersInfos;
}
//
//  End of: AST_SystemModelGenerator::FindSelectorRegisters
//---------------------------------------------------------------------------



//! Follows top module source signal path
//!
//! @param module       Top module
//! @param scanOutPort  Top module output port to follow backward to create SystemModelNode
//!
void AST_SystemModelGenerator::FollowTopModulePath (AST_Module* topModule, const AST_Port* scanOutPort)
{
  auto      module                    = topModule;
  auto      source                    = scanOutPort->Source();
  auto      sourceSignals             = std::cref(source->Signals());
  auto      isSourcedByTopModuleInput = false;
  AST_Port* scanInPort                = nullptr;

  std::tie(isSourcedByTopModuleInput, scanInPort) = IsSourcedByModuleInput(topModule, sourceSignals);

  while (   (module != nullptr)
         && (!isSourcedByTopModuleInput || !m_linkersContext.empty()))
  {
    if (isSourcedByTopModuleInput)  // ==> !m_linkersContext.empty()
    {
      CHECK_VALUE_EQ(m_instancesContext.size(), 1u, "When reaching top module there should be only 1 instance context left, got "s.append(std::to_string(m_instancesContext.size())));

      std::tie(module, sourceSignals) = Process_ScanMux_EndOfSelectionPath(nullptr);
      if (module == nullptr)
      {
        return;
      }
    }
    else   // ==> !isSourcedByTopModuleInput
    {
      CHECK_VALUE_EQ(sourceSignals.get().size(), 1u, "Expecting source to be driven by exactly one signal");

      auto        isSourcedByModuleInput = false;
      AST_Port*   scanInPort             = nullptr;

      std::tie(isSourcedByModuleInput, scanInPort) = IsSourcedByModuleInput(module, sourceSignals);

      if (isSourcedByModuleInput)  // ==> Must move up the module hierarchy
      {
        std::tie(module, sourceSignals) = Process_Instance_Exit(scanInPort);
      }
      else   // ==> Look connection inside current module
      {
        const auto signal      = sourceSignals.get().front();
        const auto portScope   = signal->PortScope();
        const auto identifier  = signal->PortName();

        if (portScope.empty())  // ScanRegister or ScanMux ?
        {
          auto scanRegister = module->FindScanRegister(identifier);
          if (scanRegister != nullptr)
          {
            if (!scanRegister->HasAssociatedRegister())       // Have we already gone to this path point ?
            {
              sourceSignals = Process_ScanRegister(scanRegister);
            }
            else if (!m_linkersContext.empty())
            {
              std::tie(module, sourceSignals) = Process_ScanMux_EndOfSelectionPath(scanRegister->AssociatedRegister());
            }
            else
            {
              return;
            }
          }
          else  // ScanMux or instance with implicit connection ?
          {
            auto scanMux  = module->FindScanMux(identifier);

            if (scanMux != nullptr)
            {
              if (scanMux->HasAssociatedLinker()) // ==> If we already passed onto that linker, we must be in context of another linker processing
              {
                std::tie(module, sourceSignals) = Process_ScanMux_EndOfSelectionPath(scanMux->AssociatedLinker());
              }
              else
              {
                std::tie(module, sourceSignals) = Process_ScanMux_Entry(scanMux, module);
              }
            }
            else   // ==> Instance with implicit connection
            {
              auto instance = module->FindInstance(identifier);
              CHECK_VALUE_NOT_NULL(instance, "Failed to find source entity \"" + signal->AsText() + "\" (not a ScanRegister, ScanMux nor Instance with implicit connection)");

              auto      instanceModule = instance->UniquifiedModule();
              AST_Port* port           = nullptr;

              const auto& scanOutPorts = instanceModule->ScanOutPorts();
              if (!scanOutPorts.empty())
              {
                CHECK_VALUE_EQ(scanOutPorts.size(), 1u, "Found usage of implicit connection of \"" + signal->AsText() + "\" with more than one ScanOutPort ==> this is not yet supported");
                port = scanOutPorts.front();
              }
              else
              {
                const auto& dataOutPorts = instanceModule->DataOutPorts();
                if (dataOutPorts.empty())
                {
                  CHECK_VALUE_NOT_NULL(instance, "Failed to find ScanOutPort or DataOutPort for instance identified by signal \"" + signal->AsText() + "\"");
                }

                CHECK_VALUE_EQ(scanOutPorts.size(), 1u, "Found usage of implicit connection of \"" + signal->AsText() + "\" with more than one DataOutPort ==> this is not yet supported");
                port = dataOutPorts.front();
              }

              std::tie(module, sourceSignals) = Process_Instance_Entry(instance, instanceModule, port);
            }
          }
        }
        else   // Instance ?
        {
          CHECK_VALUE_EQ(portScope.size(), 1u, "Expecting to have single scope depth for instance");

          auto scope    = portScope.front();
          auto instance = module->FindInstance(scope);
          CHECK_VALUE_NOT_NULL(instance, "Failed to find source entity (not an Instance)");

          auto instanceModule = instance->UniquifiedModule();
          auto scanOutPort    = instanceModule->FindScanOutPort(identifier);

          std::tie(module, sourceSignals) = Process_Instance_Entry(instance, instanceModule, scanOutPort);
        }

        if (module == nullptr)
        {
          return;
        }

        std::tie(isSourcedByModuleInput, scanInPort) = IsSourcedByModuleInput(module, sourceSignals);
        if (isSourcedByModuleInput && (m_instancesContext.size() > 1u))
        {
          std::tie(module, sourceSignals) = Process_Instance_Exit(scanInPort);
        }
      } // End of: !isSourcedByModuleInput
    } // End of: !isSourcedByTopModuleInput

    isSourcedByTopModuleInput = false;
    if (module == m_network->TopModule())
    {
      std::tie(isSourcedByTopModuleInput, scanInPort) = IsSourcedByModuleInput(topModule, sourceSignals);
    }
  }
}
//
//  End of: AST_SystemModelGenerator::FollowTopModulePath
//---------------------------------------------------------------------------


//! Generates a SystemModel (sub-)tree from AST_Network
//!
//! @param network  Scan network to convert to MAST SystemModel
//!
shared_ptr<mast::ParentNode> AST_SystemModelGenerator::Generate (AST_Network* network)
{
  m_network = CHECK_PARAMETER_NOT_NULL(network, "Expect valid, not nullptr AST_Network");

  AST_Module::ResetCircularDependencyTracker();

  m_parsedTopNode = Generate_Network(network);

  return m_parsedTopNode;
}
//
//  End of: AST_SystemModelGenerator::Generate
//---------------------------------------------------------------------------




//! Creates complete SystemModel from AST_Network
//!
//! @param network  Network description
//!
//! @return Created SystemModel sub-tree
//!
shared_ptr<mast::ParentNode> AST_SystemModelGenerator::Generate_Network (AST_Network* network)
{
  auto topModule = network->TopModule();
  CHECK_VALUE_NOT_NULL(topModule, "Cannot generate SystemModel nodes when network has no modules");

  shared_ptr<ParentNode> topNode;

  if (topModule->HasAccessLink())
  {
    auto accessLink = topModule->AccessLink();
    auto type       = accessLink->Type();
    switch (type)
    {
      case AccessLinkType::STD_1149_1_2001:
      case AccessLinkType::STD_1149_1_2013:
        LOG(INFO) << "Creating STD_1149 AccessLink";
        topNode = Generate_JTAGTap(topModule);
        break;
      case AccessLinkType::Generic:
        LOG(INFO) << "Creating Generic" << accessLink->GenericIdentifier()->AsText() << " AccessLink";
        CHECK_FAILED("Not Yet Supported: Generic AccessLink");
        break;
      default:
        CHECK_FAILED("Unexpected AccessLink type");
        break;
    }
  }
  else
  {
    auto name  = topModule->Name();
    auto chain = m_systemModel->CreateChain(name);

    Generate_TopModule(chain.get(), topModule);

    topNode = chain;
  }

  // ---------------- Unresolved Path Selector
  //
  ResolveUnresolvedPathSelectors();

  // ---------------- Algorithm Associations
  //
  m_algorithmAssociations = AST_AttributesConverter::CollectAlgorithms(nullptr, topModule, topNode);

  // ---------------- Aliases
  //
  AST_AliasConverter::ConvertAliases(topModule, topNode.get());

  return topNode;
}
//
//  End of: AST_SystemModelGenerator::Generate_Network
//---------------------------------------------------------------------------


//! Creates a SystemModel nodes for network top module
//!
//! @param topModule  Network top module with AccessLink to be converted to SystemModel nodes
//!
shared_ptr<ParentNode> AST_SystemModelGenerator::Generate_JTAGTap (AST_Module* topModule)
{
  CHECK_PARAMETER_NOT_NULL(topModule, "Need a valid \"top\" module");
  auto accessLink = topModule->AccessLink();
  CHECK_PARAMETER_NOT_NULL(accessLink, "\"Top\" module must have a valid AccessLink");

  // ---------------- Capture BSDL file name
  //
  auto bsdlFileRef = accessLink->BSDL();
  CHECK_VALUE_NOT_NULL(bsdlFileRef, "AccessLink must refer to a valid BSDL name");
  auto bsdlFileName = bsdlFileRef->Name();

  // ---------------- Capture BSDL instructions names
  //
  vector<string_view> instructionsNames;

  const auto& bsdlInstructionsRef = accessLink->BsdlInstructionsRef();
  for (const auto bsdlInstruction : bsdlInstructionsRef)
  {
    instructionsNames.push_back(bsdlInstruction->Name());
  }

  // ---------------- Parse BSDL
  //
  auto bsdlFilePath = ResolveBSDL_FilePath(bsdlFileName);
  auto bsdlContent  = Utility::ReadTextFile(bsdlFilePath);

  BSDL_Reader bsdlReader;
  bsdlReader.Parse(bsdlContent, instructionsNames);
  const auto& selectTableValues = bsdlReader.SelectTable();

  vector<BinaryVector> selectTable;
  for (const auto& selectValue : selectTableValues)
  {
    selectTable.push_back(BinaryVector::CreateFromBinaryString(selectValue));
  }

  // ---------------- Create TAP node
  //
  const auto& tapName       = accessLink->Name();
  auto        irBitsCount   = bsdlReader.IrBitsCount();
  auto        muxPathsCount = bsdlInstructionsRef.size() + 1u;  // +1 is for the bypass register
  auto        protocol      = Create_Protocol(topModule);

  auto tap = m_builder->Create_JTAG_TAP(tapName, irBitsCount, muxPathsCount, std::move(protocol), selectTable);

  // ---------------- Build nodes below tap
  //
  Generate_JTAGTapChildren(tap.get(), topModule, bsdlInstructionsRef);

  return tap;
}
//
//  End of: AST_SystemModelGenerator::Generate_JTAGTap
//---------------------------------------------------------------------------


//! Generates SystemModel sub-tree under JTAG TAP
//!
//! @param tap                  Tap AccessInterface
//! @param topModule            Module in which AccessLink is defined
//! @param bsdlInstructionsRef  BSDL instruction references (tells which ScanInterface to deal with)
//!
void AST_SystemModelGenerator::Generate_JTAGTapChildren (AccessInterface*                            tap,
                                                         AST_Module*                                 topModule,
                                                         const std::vector<AST_BsdlInstructionRef*>& bsdlInstructionsRef)
{
  InstanceContext context;
  context.instance          = nullptr;    // Instance is implicit
  context.parentModule      = topModule;
  context.parentNode        = tap;
  context.createdNodesLevel = 0;
  m_instancesContext.push(context); // Context is popped when path reaches instance input


  for (const auto bsdlInstruction : bsdlInstructionsRef)
  {
    const auto& scanInterfacesRef = bsdlInstruction->ScanInterfacesRef();

    for (const auto scanInterfaceRef : scanInterfacesRef)
    {
      const auto& interfacesNames = scanInterfaceRef->ScanInterfaceNames();
      CHECK_FALSE(interfacesNames.size() > 1u, "Only support one ScanInterface per BSDL instruction");

      const auto& scopedInterfaceName = interfacesNames.front();
      const auto  instanceRef         = std::get<0>(scopedInterfaceName);
      const auto& scanInterfaceName   = std::get<1>(scopedInterfaceName);

      AST_ScanInterface* scanInterface  = nullptr;
      AST_Module*        instanceModule = nullptr;
      AST_Instance*      instance       = nullptr;

      std::tie(instance, instanceModule, scanInterface) = FindScanInterface(topModule, instanceRef, scanInterfaceName);

      auto scanOutPort = FindScanOutPort(instanceModule, scanInterface);

      CHECK_VALUE_EMPTY(m_linkersContext, "No linker should be processed when dealing with top node ScanOutPort");

      if (instance != nullptr)
      {
        Process_Instance_Entry(instance, instanceModule, scanOutPort);

        FollowTopModulePath(instanceModule, scanOutPort);
      }
      else
      {
        FollowTopModulePath(topModule, scanOutPort);
      }

      AppendCreatedNodesToParent(tap, 0u);
    }
  }
}
//
//  End of: AST_SystemModelGenerator::Generate_JTAGTapChildren
//---------------------------------------------------------------------------


//! Creates a SystemModel nodes for network top module
//!
//! @param chain      Chain in which SystemModel nodes are created
//! @param topModule  Network top module to be converted to SystemModel nodes
//!
void AST_SystemModelGenerator::Generate_TopModule (mast::Chain* chain, AST_Module* topModule)
{
  const auto& scanInPorts  = topModule->ScanInPorts();
  const auto& scanOutPorts = topModule->ScanOutPorts();

  CHECK_VALUE_NOT_EMPTY(scanInPorts,  "Expecting a top module to have at least one ScanInPort");
  CHECK_VALUE_NOT_EMPTY(scanOutPorts, "Expecting a top module to have at least one ScanOutPort");

  InstanceContext context;

  context.instance          = nullptr;    // Instance is implicit
  context.parentModule      = topModule;
  context.parentNode        = chain;
  context.createdNodesLevel = 0;

  m_instancesContext.push(context); // Context is popped when path reaches instance input

  for (const auto& scanOutPort : scanOutPorts)
  {
    CHECK_VALUE_EMPTY(m_linkersContext, "No linker should be processed when dealing with top node ScanOutPort");

    FollowTopModulePath(topModule, scanOutPort);
    AppendCreatedNodesToParent(chain, 0u);
  }
}
//
//  End of: AST_SystemModelGenerator::Generate_TopModule
//---------------------------------------------------------------------------



//! Tells whether some signals are connected to one of module ScanInPort
//!
//! @param module   Module in which signals are defined
//! @param signals  Signals to test for connection to module ScanInPort (only one is supported)
//!
tuple<bool, AST_Port*> AST_SystemModelGenerator::IsSourcedByModuleInput (const AST_Module* module, const vector<AST_Signal*>& signals) const
{
  CHECK_VALUE_EQ(signals.size(), 1u, "Expecting source to be driven by exactly one signal, got "s.append(std::to_string(signals.size())));

  const auto  signal            = signals.front();
  const auto& moduleScanInPorts = module->ScanInPorts();
  const auto  portScope         = signal->PortScope();
  const auto  identifier        = signal->PortName();

  if (!portScope.empty())
  {
    return make_tuple(false, nullptr);
  }

  auto pos = std::find_if(moduleScanInPorts.cbegin(),
                          moduleScanInPorts.cend(),
                          [name = identifier->Name()](auto item) { return item->Name() == name; });

  bool isSourcedByModuleInput = pos != moduleScanInPorts.cend();

  return isSourcedByModuleInput ? make_tuple(true,  *pos)
                                : make_tuple(false, nullptr);
}
//
//  End of: AST_SystemModelGenerator::IsSourcedByModuleInput
//---------------------------------------------------------------------------




//! Process "entering" in a module instance
//!
//! @param instance         Instance to process
//! @param instanceModule   Module that defines the instance
//! @param scanOutPort      Port by which scan path is followed (backward)
//!
//! @return New processing context
//!
AST_SystemModelGenerator::ProcessingContext_t
AST_SystemModelGenerator::Process_Instance_Entry (AST_Instance* instance, AST_Module* instanceModule, const AST_Port* scanOutPort)
{
  const auto& moduleInputPorts = instanceModule->ScanInPorts();
  CHECK_VALUE_NOT_EMPTY(moduleInputPorts,  "Expecting an instance module \""s.append(instanceModule->Name()).append("\"to have at least one ScanInPort"));

  InstanceContext context;

  context.instance     = instance;
  context.parentModule = instanceModule;
  context.parentNode   = m_instancesContext.top().parentNode; // By default, there is no change in parent node (for case when no Chain is created)

  // ---------------- Create a Chain as necessary
  //
  auto chain = instance->AssociatedChain();
  if (!chain)
  {
    // ---------------- Create Chain to "encapsulate" instance sub-nodes
    //
    auto name = instance->Name();
    chain     = m_systemModel->CreateChain(name);

    instance->AssociatedChain(chain);   // To detect already created Chain for the instance
    AssignNewNode(chain);
    context.parentNode = chain.get();
  }
  else if (instanceModule->IsScanOutPortMarked(scanOutPort))
  {
    CHECK_VALUE_NOT_EMPTY(m_linkersContext, "Unexpected path again through instance output port outside of Linker context");

    return Process_ScanMux_EndOfSelectionPath(instance->AssociatedChain());
  }

  instanceModule->MarkScanOutPort(scanOutPort);

  context.createdNodesLevel = m_createdNodes.size();

  m_instancesContext.push(context);

  // ---------------- Return signal to follow to reach first instance entity
  //
  auto        source        = scanOutPort->Source();
  const auto& sourceSignals = source->Signals();

  return make_tuple(instanceModule, cref(sourceSignals));
}
//
//  End of: AST_SystemModelGenerator::Process_Instance
//---------------------------------------------------------------------------



//! Processes the fact that we reach a ScanInPort of an instance/module.
//! This include appending created children while processing this instance (when possible)
//!
//! @param scanInPort   Port by which the instance input was reached
//!
//! @return New processing context
AST_SystemModelGenerator::ProcessingContext_t
AST_SystemModelGenerator::Process_Instance_Exit (AST_Port* scanInPort)
{
  CHECK_VALUE_NOT_EMPTY(m_instancesContext, "Houps not balanced push/pop on instance context");

  auto& context           = m_instancesContext.top();
  auto  instance          = context.instance;
  auto  portIdentifier    = scanInPort->Identifier();
  auto  instanceInputPort = instance->FindInputPort(portIdentifier);

  if (instanceInputPort == nullptr)   // Is this special top level instance referred by an AccessLink ?
  {
    if (context.linkerNode != nullptr)
    {
      return Process_ScanMux_EndOfSelectionPath(instance->AssociatedChain());
    }
    auto instanceModule = instance->UniquifiedModule();
    auto scanInPort     = instanceModule->FindScanInPort(portIdentifier);
    CHECK_PARAMETER_NOT_NULL(scanInPort, "Cannot find instance module Port: \""s.append(portIdentifier->Name()).append("\""));

    m_instancesContext.pop();
    CHECK_VALUE_NOT_EMPTY(m_instancesContext, "Must remain at least one (implicit) instance context for top module");
    return make_tuple(nullptr, cref(sm_noSignals));   // This is to report end of scan path !
  }

  auto        instanceSource = instanceInputPort->Source();
  const auto& sourceSignals  = instanceSource->Signals();

  // ---------------- Restore previous context
  //
  m_instancesContext.pop();
  CHECK_VALUE_NOT_EMPTY(m_instancesContext, "Must have at least an (implicit) instance context for top module");

  auto module = m_instancesContext.top().parentModule;

  return make_tuple(module, cref(sourceSignals));
}
//
//  End of: AST_SystemModelGenerator::Process_InstanceExit
//---------------------------------------------------------------------------




//! Creates a SystemModel Linker from an AST_ScanMux (with temporary path selector)
//!
//! @param scanMux  ScanMux to be converted to SystemModel Linker
//! @param module   ScanMux parent module
//!
//! @return ScanMux source signals for 1st selection
//!
AST_SystemModelGenerator::ProcessingContext_t
AST_SystemModelGenerator::Process_ScanMux_Entry (AST_ScanMux* scanMux, AST_Module* module)
{
  CHECK_FALSE(scanMux->IsBusMux(),            "Not Yet Supported: ScanMux for buses");
  CHECK_FALSE(scanMux->HasAssociatedLinker(), "Cannot create another linker for ScanMux \""s.append(scanMux->BaseName()).append("\""));

  // ---------------- Create Linker
  //
  auto        unresolvedPathSelector = make_shared<UnresolvedPathSelector>();
  const auto& name                   = scanMux->BaseName();
  auto        linker                 = m_systemModel->CreateLinker(name, unresolvedPathSelector);

  scanMux->AssociatedLinker(linker);

  AssignNewNode(linker);

  auto& instanceContext  = m_instancesContext.top();
  auto  linkerParentNode = instanceContext.parentNode;
//+  auto  linkerParentNode = (instanceContext.linkerNode != nullptr) ? instanceContext.linkerNode
//+                                                                   : instanceContext.parentNode;

  instanceContext.linkerNode = linker.get();

  LinkerContext linkerContext;

  linkerContext.instancesContext       = m_instancesContext;
  linkerContext.processedScanMux       = scanMux;
  linkerContext.processedSelectionId   = 0;
  linkerContext.nodesLevelAfterCreate  = m_createdNodes.size();
  linkerContext.nodesLevelAfter1stPath = 0u;
  linkerContext.linker                 = linker.get();
  linkerContext.linkerParentNode       = linkerParentNode;

  m_linkersContext.push_back(std::move(linkerContext));

  const auto& sourceSignals = scanMux->Selections().front()->SelectedSignals();
  return make_tuple(module, cref(sourceSignals));
}
//
//  End of: AST_SystemModelGenerator::Process_ScanMux_Entry
//---------------------------------------------------------------------------






//! Does what is needed to process when we reached end of ScanMux selection
//!
//! @param commonLinkerNode   The first, common, SystemModelNode encounter following all linker selections path
//!
//! @return New processing context
//!
AST_SystemModelGenerator::ProcessingContext_t
AST_SystemModelGenerator::Process_ScanMux_EndOfSelectionPath (shared_ptr<mast::SystemModelNode> commonLinkerNode)
{
  CHECK_VALUE_NOT_EMPTY(m_linkersContext, "There is no linker context to process");

  auto&       linkerContext          = m_linkersContext.back();
  auto        linker                 = linkerContext.linker;
  auto        scanMux                = linkerContext.processedScanMux;
  auto        nodesLevelAfter1stPath = linkerContext.nodesLevelAfter1stPath;
  auto        processedSelectionId   = linkerContext.processedSelectionId;
  auto const& selections             = scanMux->Selections();
  auto        module                 = linkerContext.instancesContext.top().parentModule;

  // ---------------- Save Common Linker Node in proper context (most recent Linker processing id 1)
  //
  if (commonLinkerNode != nullptr)
  {
    auto contextPos = std::find_if(m_linkersContext.rbegin(), m_linkersContext.rend(), [](const auto& context) { return context.processedSelectionId == 1u; });
    if (contextPos != m_linkersContext.rend())
    {
      if (contextPos->commonLinkerNode == nullptr)
      {
        contextPos->commonLinkerNode = commonLinkerNode.get();
      }
      else
      {
        CHECK_VALUE_EQ(contextPos->commonLinkerNode, commonLinkerNode.get(), "Houps, expecting same Linker \"common node\" ");
      }
    }
  }


  CHECK_VALUE_GTE(m_createdNodes.size(), nodesLevelAfter1stPath, "Have appended more nodes than expected");
  auto newCreated = m_createdNodes.size() - nodesLevelAfter1stPath;

  if (processedSelectionId == 0)
  {
    linkerContext.nodesLevelAfter1stPath = m_createdNodes.size();
  }
  else if (newCreated > 1u)
  {
    auto chain = Create_ChainForLinker(linker, processedSelectionId);
    linker->AppendChild(chain);
    AppendCreatedNodesToParent(chain.get(), nodesLevelAfter1stPath);
  }
  else if (newCreated != 0)
  {
    AppendCreatedNodesToParent(linker, nodesLevelAfter1stPath);
  }

  ++processedSelectionId;

  m_instancesContext = linkerContext.instancesContext;

  bool lastLinkerSelection = processedSelectionId >= selections.size();
  if (lastLinkerSelection)
  {
    auto firstSelectionIsEmpty = AssignNodesToLinkerFirstSelection();

    // ---------------- Assign proper PathSelector
    //
    auto pathSelector = Create_PathSelector(scanMux, module, firstSelectionIsEmpty);
    linker->ReplacePathSelector(pathSelector);

    // ---------------- Deal with parent nodes
    //
    auto parentNode = linkerContext.linkerParentNode;

    auto& instanceContext = m_instancesContext.top();
    instanceContext.parentNode = parentNode;
    instanceContext.linkerNode = nullptr;

    auto sourceSignals = std::cref(selections.front()->SelectedSignals()); // Will cause detection of instance input port or previously created node that is just before the linker

    m_linkersContext.pop_back();
    if (m_linkersContext.empty())
    {
      return make_tuple(nullptr, cref(sourceSignals));  // To report end of path processing
    }

    return Process_ScanMux_EndOfSelectionPath(nullptr); // Recursion point !
  }

  ++linkerContext.processedSelectionId;
  auto sourceSignals = std::cref(selections[processedSelectionId]->SelectedSignals());
  return make_tuple(module, cref(sourceSignals));
}
//
//  End of: AST_SystemModelGenerator::Process_ScanMux_EndOfSelectionPath
//---------------------------------------------------------------------------


//! Creates a SystemModel Register from an AST_ScanRegister
//!
//! @param scanRegister   ScanRegister to be converted to SystemModel Register
//!
//! @return Signals to follow to reach previous entity in test network (moving backward)
//!
AST_SystemModelGenerator::SourceSignalsRef_t
AST_SystemModelGenerator::Process_ScanRegister (AST_ScanRegister* scanRegister)
{
  auto source      = scanRegister->ScanInSource();
  auto name        = scanRegister->BaseName();
  auto bitsCount   = scanRegister->BitsCount();
  auto resetValue  = scanRegister->ResetValue();
  auto bypassValue = BinaryVector(bitsCount, 0);

  if (resetValue != nullptr)
  {
    try
    {
      bypassValue = resetValue->AsBinaryVector(bitsCount);
    }
    catch(std::invalid_argument&)
    {
      auto message = "Reset value "s;
      message += resetValue->IsFullySized() ? resetValue->AsBinaryVector().DataAsICLMixString()
                                            : resetValue->AsText();
      message += " is too large for register \"" + name + "\" with " + std::to_string(bitsCount) + " bit(s)";
      CHECK_FAILED(message);
    }
  }
  bypassValue.FixSize(true);

  auto holdValue    = false;
  auto registerNode = m_systemModel->CreateRegister(name, bypassValue, holdValue);

  scanRegister->AssociatedRegister(registerNode);

  AssignNewNode(registerNode);

  return std::cref(source->Signals());
}
//
//  End of: AST_SystemModelGenerator::Process_ScanRegister
//---------------------------------------------------------------------------


//! Creates selection/deselection tables for ScanMux (for table based PathSelector)
//!
//! @param selections             ScanMux selection values
//! @param expectedBitsCount      Bits count of selector register(s) - this is used only for value width check
//! @param firstSelectionIsEmpty  When true, first mux selection is ignored in selection/deselection tables (and the linker must be set with can_select_none = true)
//!
AST_SystemModelGenerator::SelectionTables_t AST_SystemModelGenerator::MakeSelectionTable(const vector<AST_ScanMuxSelection*>& selections,
                                                                                         uint32_t                             expectedBitsCount,
                                                                                         bool                                 firstSelectionIsEmpty)
{
  vector<BinaryVector> selectTable;
  selectTable.emplace_back(expectedBitsCount); // Dummy entry for not used path identifier zero

  auto skip = firstSelectionIsEmpty;

  for (const auto& selection : selections)
  {
    if (skip)
    {
      skip = false;
      continue;
    }

    const auto& values = selection->SelectionsValues();
    CHECK_VALUE_NOT_EMPTY(values, "Must have at least one value");
    CHECK_VALUE_EQ(values.size(), 1u, "Not Yet Supported: Concat number list (for ScanMux Selection)");

    const auto value               = values.front();
    auto       valueAsBinaryVector = value->AsBinaryVector(expectedBitsCount);
    selectTable.emplace_back(std::move(valueAsBinaryVector));
  }

  //! @todo [JFC]-[November/17/2017]: Support Concat number list in AST_SystemModelGenerator::MakeSelectionTable()

  auto isFirst = true;
  vector<BinaryVector> deselectTable = selectTable;
  for (auto& deselectValue : deselectTable)
  {
    if (isFirst)
    {
      isFirst = false;
    }
    else
    {
      deselectValue.ToggleBits();
    }
  }

  return make_tuple(selectTable, deselectTable);
}
//
//  End of: AST_SystemModelGenerator::MakeSelectionTable
//---------------------------------------------------------------------------



//! Resolves yet unresolved path selector (those for which no SystemModel Register was not not create from ScanRegister
//!
void AST_SystemModelGenerator::ResolveUnresolvedPathSelectors ()
{
  for (const auto& info : m_unresolvedPathSelectorsInfos)
  {
    CHECK_PARAMETER_NOT_NULL(info.scanMux, "Houps: UnresolvedPathSelectorInfo has been badly initialized");

    auto linker = info.scanMux->AssociatedLinker();
    CHECK_VALUE_NOT_NULL(linker, "Houps: Cannot resolve a path selector when no Linker is associated with ScanMux \"" + info.scanMux->Name() + "\"");

    // ---------------- Create a proper PathSelector
    //
    const auto& selectorRegistersInfos = info.selectorRegistersInfos;
    auto        unresolvedPathSelector = info.unresolvedPathSelector;
    auto        properties             = unresolvedPathSelector->Properties();

    auto pathSelector = Create_PathSelector(selectorRegistersInfos,
                                            unresolvedPathSelector->MovedSelectTable(),
                                            unresolvedPathSelector->MovedDeselectTable(),
                                            properties);

    // ---------------- Replace Linker temporary (unresolved) PathSelector
    //
    linker->ReplacePathSelector(pathSelector);
  }
}
//
//  End of: AST_SystemModelGenerator::ResolveUnresolvedPathSelectors
//---------------------------------------------------------------------------



//! Resolves actual path for BSDL file
//!
//! @param bsdlName   Name for BSDL file found in AccessLink
//!
string AST_SystemModelGenerator::ResolveBSDL_FilePath (const string& bsdlName)
{
  // ---------------- Try with just the name
  //
  if (Utility::FileExists(bsdlName))
  {
    return bsdlName;
  }

  // ---------------- Try using search paths (with or without forced .bsd extension)
  //
  for (const auto& searchPath : m_filesSearchPaths)
  {
    auto filePath = searchPath;
    filePath.append(DIRECTORY_SEPARATOR).append(bsdlName);

    if (Utility::FileExists(filePath))
    {
      return filePath;
    }

    filePath.append(".bsd");
    if (Utility::FileExists(filePath))
    {
      return filePath;
    }
  }

  CHECK_FAILED("Cannot find BSDL file with name: \""s.append(bsdlName).append("\""));
}
//
//  End of: AST_SystemModelGenerator::ResolveBSDL_FilePath
//---------------------------------------------------------------------------



//===========================================================================
// End of AST_SystemModelGenerator.cpp
//===========================================================================
