//===========================================================================
//                           AST_ScanRegister.cpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file AST_ScanRegister.cpp
//!
//! Implements class AST_ScanRegister
//!
//===========================================================================

#include "AST_ScanRegister.hpp"
#include "AST_VectorIdentifier.hpp"
#include "AST_PlaceHolder.hpp"
#include "AST_ConcatNumber.hpp"
#include "AST_Signal.hpp"
#include "AST_Source.hpp"
#include "AST_Visitor.hpp"
#include "AST_Builder.hpp"
#include "Utility.hpp"

using std::vector;
using std::string;
using std::experimental::string_view;

using namespace std::experimental::literals::string_view_literals;
using namespace std::string_literals;
using namespace Parsers;

//! Visited part of the Visitor pattern
//!
void AST_ScanRegister::Accept (AST_Visitor& visitor)
{
  visitor.Visit_ScanRegister(this);
}



//! Returns ScanRegister bits count
//!
size_t AST_ScanRegister::BitsCount () const
{
  return static_cast<size_t>(m_identifier->BitsCount());
}
//
//  End of: AST_ScanRegister::BitsCount
//---------------------------------------------------------------------------



//! Dispatches children to specific member (for ease of use)
//!
void AST_ScanRegister::DispatchChildren ()
{
  for (auto&  child:  UndispatchedChildren())
  {
    if (child != nullptr)
    {
      switch (child->GetKind())
      {
        case Parsers::Kind::ScanInSource:  SetChild(child, m_scanInSource);  break;
        case Parsers::Kind::CaptureSource: SetChild(child, m_captureSource); break;
        case Parsers::Kind::PlaceHolder:
        {
          auto placeHolder = static_cast<AST_PlaceHolderBase*>(child);
          auto targetKind  = placeHolder->TargetKind();

          switch (targetKind)
          {
            case Parsers::Kind::ResetValue:       SetChild(child, m_resetValue);       break;
            case Parsers::Kind::DefaultLoadValue: SetChild(child, m_defaultLoadValue); break;
            default: // Ignore all other for now
              break;
          }
          child = nullptr;  // "Discard" all other place holders
          break;
        }
        default:  // Ignore all other for now
          break;
      }
    }
  }

  CHECK_VALUE_NOT_NULL(m_scanInSource, "The source of the scan input data for a ScanRegister \""s.append(m_identifier->AsText()).append("\" is required") );
}
//
//  End of: AST_ScanRegister::DispatchChildren
//---------------------------------------------------------------------------


//! Returns scan register left index (when not defined as a scalar identifier)
//!
uint32_t AST_ScanRegister::RangeLeft () const
{
  return m_identifier->LeftIndex();
}
//
//  End of: AST_ScanRegister::RangeLeft
//---------------------------------------------------------------------------


//! Returns scan register right index (when not defined as a scalar identifier)
//!
uint32_t AST_ScanRegister::RangeRight () const
{
  return m_identifier->RightIndex();
}
//
//  End of: AST_ScanRegister::RangeRight
//---------------------------------------------------------------------------



//! Replaces parameter references with their actual value, then resolve value expressions
//!
//! @param parameters   Actual parameter values - There should be no parameter reference in their values
//!
void AST_ScanRegister::Resolve (const vector<AST_Parameter*>& parameters)
{
  m_identifier->Resolve(parameters);

  if (m_resetValue != nullptr)
  {
    m_resetValue->Resolve(parameters);
  }

  if (m_defaultLoadValue != nullptr)
  {
    m_defaultLoadValue->Resolve(parameters);
  }
}
//
//  End of: AST_ScanRegister::Resolve
//---------------------------------------------------------------------------


//! ScanRegister source base name (without indices)
//!
const string& AST_ScanRegister::SourceBaseName () const
{
  const auto& signals = m_scanInSource->Signals();

  CHECK_VALUE_EQ(signals.size(), 1u, "Expecting ScanOutPort source to be drive by exactly one signal");
  const auto signal      = signals.front();
//+  const auto portScope   = signal->PortScope();
  const auto identifier  = signal->PortName();

  const auto& name = identifier->Name();
  return name;
}
//
//  End of: AST_ScanRegister::SourceBaseName
//---------------------------------------------------------------------------




//! Returns uniquified clone of value or string expression
//!
//! @param astBuilder   Interface to clone some kind of AST nodes (it is responsible for the memory management)
//!
//! @return New cloned and uniquified AST_ScanRegister
AST_ScanRegister* AST_ScanRegister::UniquifiedClone (AST_Builder& astBuilder) const
{
  auto cloned = astBuilder.Clone_ScanRegister(this);

  if (m_identifier->HasParameterRef())
  {
    cloned->m_identifier = m_identifier->UniquifiedClone(astBuilder);
  }

  if ((m_resetValue != nullptr) && m_resetValue->HasParameterRef())
  {
    cloned->m_resetValue = m_resetValue->UniquifiedClone(astBuilder);
  }

  if ((m_defaultLoadValue != nullptr) && m_defaultLoadValue->HasParameterRef())
  {
    cloned->m_defaultLoadValue = m_defaultLoadValue->UniquifiedClone(astBuilder);
  }

  return cloned;
}
//
//  End of: AST_ScanRegister::UniquifiedClone
//---------------------------------------------------------------------------


//===========================================================================
// End of AST_ScanRegister.cpp
//===========================================================================
