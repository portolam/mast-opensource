//===========================================================================
//                           EmptyInit.cpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file EmptyInit.cpp
//!
//! Implements Init function for examples that do not need any special initialization
//!
//===========================================================================


void Init (int argc, char* argv []);

//! Doe special init for specific example (can do nothing)
//!
void Init (int /* argc */, char* /* argv */ [])
{
}

//===========================================================================
// End of EmptyInit.cpp
//===========================================================================
