//===========================================================================
//                           ml505_spi.cpp
//===========================================================================
//! @file ml505_spi.cpp
//!
//! Increment the ML505 SPI data register via libftdispi.
//!
//===========================================================================

#include "CPP_API.hpp"

#include <string>
#include <vector>


using std::vector;
using std::string;

using namespace std::string_literals;
using namespace mast;

std::string                               GetSitModelFilePath       (int argc, char* argv []);
std::vector<mast::AppFunctionAndName>     GetAlgorithmsNames        (int argc, char* argv []);
std::vector<mast::AppFunctionAndNodePath> GetAlgorithmsAndNodePaths (int argc, char* argv []);


namespace
{
  //! This algorithm increments "reg_data"
  //!
  void Algo_Increment ()
  {
    auto     registerPath = "reg_data";
    auto     loopCount    = 90u;
    uint16_t initialValue = 1u;

    while (loopCount--)
    {
      iWrite(registerPath, initialValue);
      iApply();

      ++initialValue;
    }
  }
  //
  //  End of: Algo_Increment
  //---------------------------------------------------------------------------

  //
  //  End of: Algo_Increment
  //---------------------------------------------------------------------------


} // End of unnamed namespace



//! Returns descriptions for applications to run on Example board
//!
//! @note Adapt this function as necessary
//!       It must nevertheless be compliant with SystemModel (possibly with 'SIT' file)
vector<AppFunctionAndNodePath> GetAlgorithmsAndNodePaths (int /* argc */, char* /* argv */[])
{
  vector<AppFunctionAndNodePath> associations
  {
    {Algo_Increment, "", "Inc"},
  };

  return associations;
}
//
//  End of: test::Example_CreateApplicationsDescriptor
//---------------------------------------------------------------------------


//! Returns names of provided applications.
//!
//! @param argc   Number of arguments provided to main function
//! @param argv   Arguments provided to main function
//!
//! @note This is intended to be match names associated with node in SIT files
//!
vector<mast::AppFunctionAndName> GetAlgorithmsNames (int /* argc */, char* /* argv */[])
{
  vector<AppFunctionAndName> names
  {
    {Algo_Increment, "Incr"},
  };

  return names;
}
//
//  End of: GetSitModelFilePath
//---------------------------------------------------------------------------


//! Returns SIT formatted file path to create a SystemModel from.
//!
string GetSitModelFilePath (int argc, char* argv[])
{
  if (argc > 1)
  {
    return string(argv[1]);
  }

  return "ml505_spi.sit";
}
//
//  End of: GetSitModelFilePath
//---------------------------------------------------------------------------


//===========================================================================
// End of ml505_spi.cpp
//===========================================================================
