//===========================================================================
//                           DefaultMastMain.hpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file DefaultMastMain.hpp
//!
//! Declares default MAST "main" application
//!
//===========================================================================


#ifndef DEFAULTMASTMAIN_H__5189BE5D_E1C2_43B5_8EA0_A279740896A0__INCLUDED_
  #define DEFAULTMASTMAIN_H__5189BE5D_E1C2_43B5_8EA0_A279740896A0__INCLUDED_


//! Runs some mast example main
//!
int DefaultMastMain (int argc, char* argv []);

#endif  // not defined DEFAULTMASTMAIN_H__5189BE5D_E1C2_43B5_8EA0_A279740896A0__INCLUDED_
//===========================================================================
// End of DefaultMastMain.hpp
//===========================================================================



