//===========================================================================
//                           Examples_Utils.hpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file Examples_Utils.hpp
//!
//! Declares Utility function to use with examples
//!
//===========================================================================


#ifndef EXAMPLES_UTILS_H__225BB14E_C90A_4FE7_10AD_73EFC91CA5E__INCLUDED_
  #define EXAMPLES_UTILS_H__225BB14E_C90A_4FE7_10AD_73EFC91CA5E__INCLUDED_

#include <string>
#include <experimental/string_view>

std::string GetFromArgs (int argc, char* argv[], std::experimental::string_view paramPrefix);

#endif  // not defined EXAMPLES_UTILS_H__225BB14E_C90A_4FE7_10AD_73EFC91CA5E__INCLUDED_
//===========================================================================
// End of Examples_Utils.hpp
//===========================================================================



