Acronyms {#Acronyms}
========

[Acronyms]:   Acronyms.html

[ABI]:   Acronyms.html "Application Binary Interface"
[API]:   Acronyms.html "Application Programming Interface"
[ATE]:   Acronyms.html "Automatic Test Equipment"
[ATE]:   Acronyms.html "Automatic Test Equipment"
[ATPG]:  Acronyms.html "Automatic Test Pattern Generator"
[BSDL]:  Acronyms.html "Boundary Scan Description Language"
[CDC]:   Acronyms.html "Clock Domain Crossing"
[CSU]:   Acronyms.html "Capture-Shift-Update"
[DLL]:   Acronyms.html "Dynamically Linked Library"
[ETS]:   Acronyms.html "European Test Symposium"
[GML]:   Acronyms.html "Graph Modelling Language"
[HDR]:   Acronyms.html ""
[HIR]:   Acronyms.html ""
[HSDL]:  Acronyms.html "Hierarchical Scan Description Language"
[ICL]:   Acronyms.html "Instrument Connectivity Language"
[IDF]:   Acronyms.html ""
[IR]:    Acronyms.html "Instruction Register"
[ITC]:   Acronyms.html "International Test Conference"
[JTAG]:  Acronyms.html "Joint Test Association Group"
[MAST]:  Acronyms.html "MAnager for System-Centric Test"
[MCP]:   Acronyms.html "Multi-Cycle Path"
[MIBs]:  Acronyms.html "Multiple Segment Insertion Bits"
[PAR]:   Acronyms.html ""
[PDL]:   Acronyms.html "Procedure Description Language"
[POC]:   Acronyms.html "Proof Of Concept"
[RPC]:   Acronyms.html "Remote Procedure Call"
[SIB]:   Acronyms.html "Segment Insertion Bit"
[JTAG]:  Acronyms.html "Joint Test Action Group"
[SJTAG]: Acronyms.html "System JTAG (working group)"
[SM]:    Acronyms.html "System Model"
[SIT]:   Acronyms.html "Simplified ICL Tree Reader"
[SPI]:   Acronyms.html "Serial Peripheral Interface"
[STIL]:  Acronyms.html "Standard Test Interface Language (For Digital Test Vectors)"
[SUT]:   Acronyms.html "System Under Test"
[SVF]:   Acronyms.html "Serial Vector Format"
[SWIR]:  Acronyms.html ""
[TAM]:   Acronyms.html "Test Access Mechanism"
[TAP]:   Acronyms.html "Test Access Point"
[TC]:    Acronyms.html "Test Controller"
[TDR]:   Acronyms.html "Test Data Register"
[TGT]:   Acronyms.html "Test Generation Tool"
[TSR]:   Acronyms.html "TDR Selection Register"
[WBY]:   Acronyms.html ""
[WI]:    Acronyms.html "Wrapped Instrument"
[WIR]:   Acronyms.html ""


Acronyms | Definitions
---------|------------
ABI      | Application Binary Interface
API      | Application Programming Interface
ATE      | Automatic Test Equipment
ATPG     | Automatic Test Pattern Generator
BSDL     | Boundary Scan Description Language
CDC      | Clock Domain Crossing
CSU      | Capture-Shift-Update
DLL      | Dynamically Linked Library
ETS      | European Test Symposium
GML      | [Graph Modelling Language](https://en.wikipedia.org/wiki/Graph_Modelling_Language)
HDR      | |
HIR      | |
HSDL     | Hierarchical Scan Description Language
ICL      | Instrument Connectivity Language
IDF      | |
IR       | Instruction Register
ITC      | International Test Conference
JTAG     | Joint Test Association Group
MAST     | MAnager for System-Centric Test
MCP      | Multi-Cycle Path
MIBs     | Multiple Segment Insertion Bits
PAR      | |
PDL      | Procedure Description Language
POC      | Proof Of Concept
RPC      | Remote Procedure Call
SIB      | Segment Insertion Bit
SIT      | Simplified ICL Tree Reader
SJTAG    | System JTAG (working group)
SM       | System Model
SPI      | Serial Peripheral Interface
STIL     | Standard Test Interface Language (For Digital Test Vectors)
SUT      | System Under Test
SVF      | Serial Vector Format
SWIR     | |
TAM      | Test Access Mechanism
TAP      | Test Access Point
TC       | Test Controller
TDR      | Test Data Register
TGT      | Test Generation Tool
TSR      | TDR Selection Register
WBY      | |
WI       | Wrapped Instrument
WIR      | |
