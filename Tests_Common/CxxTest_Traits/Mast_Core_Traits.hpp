//===========================================================================
//                           Mast_Core_Traits.hpp
//===========================================================================
// Copyright (C) 2016 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file Mast_Core_Traits.hpp
//!
//! Declares CxxTest traits for Mast_Core classes and enums. This is useful
//! to have a single file to include all traits to reduce risk of ODR (One
//! Definition Rule) violation.
//!
//===========================================================================

#ifndef MAST_CORE_TRAITS_H__D5832FFA_3D66_46E6_D6B0_DD0AFDDB868D__INCLUDED_
  #define MAST_CORE_TRAITS_H__D5832FFA_3D66_46E6_D6B0_DD0AFDDB868D__INCLUDED_

#include "Mast_Core_Enums_Traits.hpp"
#include "BinaryVector_Traits.hpp"
#include "IndexedRange_Traits.hpp"
#include "VirtualRegister_Traits.hpp"
#include "CheckResult_Traits.hpp"
#include "ParentNode_Traits.hpp"
#include "AppFunctionNameAndNode_Traits.hpp"

#endif  // not defined MAST_CORE_TRAITS_H__D5832FFA_3D66_46E6_D6B0_DD0AFDDB868D__INCLUDED_
//===========================================================================
// End of Mast_Core_Traits.hpp
//===========================================================================
