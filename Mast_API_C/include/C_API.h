//===========================================================================
//                           C_API.h
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file C_API.h
//!
//! Includes all files needed to use MAST through its 'C' language API
//!
//===========================================================================

#ifndef C_API_H__944C011A_10F_473D_A9AC_1CFAC2FF74F4__INCLUDED_
  #define C_API_H__944C011A_10F_473D_A9AC_1CFAC2FF74F4__INCLUDED_

#include "ErrorCode_C.hpp"
#include "PDL_Adapter_C.hpp"
#include "SystemModelAdapter_C.hpp"

#endif  // not defined C_API_H__944C011A_10F_473D_A9AC_1CFAC2FF74F4__INCLUDED_
//===========================================================================
// End of C_API.h
//===========================================================================



