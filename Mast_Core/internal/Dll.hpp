//===========================================================================
//                           Dll.hpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file Dll.hpp
//!
//! Declares functions for managing DLLs
//!
//===========================================================================


#ifndef DLL_H__328B051D_756A_4D35_788F_837D15FD2819__INCLUDED_
  #define DLL_H__328B051D_756A_4D35_788F_837D15FD2819__INCLUDED_

#include "Mast_Core_export.hpp"

#include <string>
#include <vector>
#include <tuple>

namespace mast
{
  class MAST_CORE_EXPORT Dll final
  {
    public:
    Dll() = delete;

    //! Returns list of DLLs in specified directory
    //!
    static std::vector<std::string> GetInDirectory(const std::string& directoryPath);

    //! Loads a Dll
    //!
    static std::string Load(const std::string& dllPath);

    //! Trie loading a DLL
    //!
    static std::string TryLoad(const std::string& pathHint, const std::string& dllPath);

    //! Returns true if file exist with or without a path hint
    //!
    static std::tuple<bool, std::string> FileExists (const std::string& pathHint, const std::string& filePath);

  };
} // End of namespace mast

#endif  // not defined DLL_H__328B051D_756A_4D35_788F_837D15FD2819__INCLUDED_

//===========================================================================
// End of Dll.hpp
//===========================================================================



