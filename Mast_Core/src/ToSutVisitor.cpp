//===========================================================================
//                           ToSutVisitor.cpp
//===========================================================================
// Copyright (C) 2016 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file ToSutVisitor.cpp
//!
//! Implements class ToSutVisitor
//!
//===========================================================================

#include "ToSutVisitor.hpp"
#include "SystemModelNodes.hpp"
#include "AccessInterfaceTranslator.hpp"

using namespace mast;



//! Stops recursing down the model hierarchy
//!
//! @note ToSutVisitor must stop at AccessInterface boundary in order to manage hierarchical processing of the model
//!       By CONSEQUENCE it MUST NOT initially be started on an AccessInterface BUT successively on its channels (children)
//!
void ToSutVisitor::VisitAccessInterface (AccessInterface& /* accessInterface */)
{
 LOG(INFO) <<"RVF : Retargeter needs to push this vector to SUT: "<<m_toSutVector.DataAsHexString();
}
//
//  End of: ToSutVisitor::VisitAccessInterface
//---------------------------------------------------------------------------


//! Visits AccessInterfaceTranslator pending children
//!
void ToSutVisitor::VisitAccessInterfaceTranslator (AccessInterfaceTranslator& accessInterfaceTranslator)
{
  VisitChildren(accessInterfaceTranslator);
}
//
//  End of: ToSutVisitor::VisitAccessInterfaceTranslator
//---------------------------------------------------------------------------


//! Visits Chain pending children
//!
void ToSutVisitor::VisitChain (Chain& chain)
{
  VisitChildren(chain);
}
//
//  End of: ToSutVisitor::VisitChain
//---------------------------------------------------------------------------

//! Visits Streamer pending children
//!
void ToSutVisitor::VisitStreamer (Streamer& streamer)
{
  Streamer_Level Current_Level;
  std::get<0>(Current_Level) = m_toSutVector.BitsCount(); //Save position of streamer INPUT
  
  VisitChildren(streamer);
  
  std::get<1>(Current_Level) = m_toSutVector.BitsCount(); //Save position of streamer OUTPUT
  std::get<2>(Current_Level) = streamer.Identifier(); //Save reference to streamer node
  m_ActiveStreamers.emplace_back(Current_Level);
}
//
//  End of: ToSutVisitor::VisitStreamer
//---------------------------------------------------------------------------



//! Visits Linker pending children
//!
void ToSutVisitor::VisitLinker (Linker& linker)
{
  VisitActiveRegisters(linker);
}
//
//  End of: ToSutVisitor::VisitLinker
//---------------------------------------------------------------------------



//! Appends Register value to send to SUT while saving the fact that it was active
//!
//! @note __Should be only used (indirectly called) for active (currently selected) Registers__
//!
void ToSutVisitor::VisitRegister (Register& reg)
{
  if (m_ignorePendingState || reg.IsPendingForWrite())
  {
    m_toSutVector.Append(reg.NextToSut());
  }
  else
  {
    m_toSutVector.Append(reg.BypassSequence());
  }
  
  if (reg.MustCheckExpected())
   { 
   m_CheckExpected = true; //Set to true if not already done
   //Prepare Expected value and Mask
   m_expectedFromSut.Append(reg.ExpectedFromSut());
   if (!reg.DontCareMask().IsEmpty())
     m_dontCareMask.Append(reg.DontCareMask());
   else 
    { 
     auto full_mask = BinaryVector(reg.BitsCount(), 0u, SizeProperty::Fixed);
     full_mask.ToggleBits();
     m_dontCareMask.Append(full_mask);
     }
   }
  else 
   { //Put all-zero bitstreams
   m_expectedFromSut.Append(BinaryVector(reg.BitsCount(), 0u, SizeProperty::Fixed));//(reg.BypassSequence());
   m_dontCareMask.Append(BinaryVector(reg.BitsCount(), 0u, SizeProperty::Fixed));
   }
  
  m_activeRegisters.emplace_back(reg.Identifier());
}
//
//  End of: ToSutVisitor::VisitRegister
//---------------------------------------------------------------------------


//===========================================================================
// End of ToSutVisitor.cpp
//===========================================================================
