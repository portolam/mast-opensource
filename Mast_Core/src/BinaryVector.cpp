//===========================================================================
//                           BinaryVector.cpp
//===========================================================================
// Copyright (C) 2016 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file BinaryVector.cpp
//!
//! Implements BinaryVector and SVFVector classes
//!
//===========================================================================

#include "BinaryVector.hpp"
#include "Utility.hpp"
#include "g3log/g3log.hpp"
#include <sstream>
#include <array>
#include <algorithm>
#include <cstring>
#include <tuple>


using std::array;
using std::vector;
using std::initializer_list;
using std::ostringstream;
using std::string;
using std::to_string;
using std::experimental::string_view;
using std::make_tuple;
using std::tie;

using namespace std::string_literals;
using namespace mast;

#define CHECK_SAME_SIZE(other)                if (other.m_usedBits != m_usedBits)         THROW_LOGIC_ERROR("BinaryVectors must have same size")
#define CHECK_NOT_EMPTY                       if (IsEmpty())                              THROW_LOGIC_ERROR("BinaryVector must not be empty")
#define CHECK_FIXED_SIZE                      if (FixedSize())                            THROW_LOGIC_ERROR("BinaryVector size has been fixed to " + to_string(m_usedBits) + " bit(s)")
#define CHECK_FIXED_SIZE_ASSIGNMENT(newSize)  if (FixedSize() && (newSize != m_usedBits)) THROW_LOGIC_ERROR("BinaryVector size has been fixed to " + to_string(m_usedBits) + " bit(s); Cannot assign " + to_string(newSize) + " bit(s)")
#define CHECK_AT_LEAST_1_BIT(numBits)         if (numBits == 0)                           THROW_INVALID_ARGUMENT("Number of bits to append must be != 0")

namespace
{
  //! Reports how a value is encoded within a string
  //!
  struct EncodingInfo
  {
    NumberBase base      = NumberBase::Undefined;
    bool       inverted  = false;
    uint32_t   bitsCount = 0; //!< Value 0 means unspecified

    EncodingInfo(NumberBase p_base = NumberBase::Undefined, bool p_inverted = false, uint32_t p_bitsCount = 0)
      : base      (p_base)
      , inverted  (p_inverted)
      , bitsCount (p_bitsCount)
    {
    }
  };


  //! Defines a mask to keep most significant bits of a uint8_t
  //!
  constexpr uint8_t LEFT_BITS_MASK_8[] =
  {
    0b00000000, // 0 bits
    0b10000000, // 1 bits
    0b11000000, // 2 bits
    0b11100000, // 3 bits
    0b11110000, // 4 bits
    0b11111000, // 5 bits
    0b11111100, // 6 bits
    0b11111110, // 7 bits
    0b11111111, // 8 bits
  };


  //! Defines a mask to clear most significant bits of a uint8_t (keeping lsb)
  //!
  constexpr uint8_t LEFT_BITS_CLEAR_MASK_8[] =
  {
    0b11111111, // 0 bits
    0b01111111, // 1 bits
    0b00111111, // 2 bits
    0b00011111, // 3 bits
    0b00001111, // 4 bits
    0b00000111, // 5 bits
    0b00000011, // 6 bits
    0b00000001, // 7 bits
    0b00000000, // 8 bits
  };

  //! Defines a mask to least most significant bits of a uint8_t
  //!
  constexpr uint8_t RIGHT_BITS_MASK_8[] =
  {
    0b00000000, // 0 bits
    0b00000001, // 1 bits
    0b00000011, // 2 bits
    0b00000111, // 3 bits
    0b00001111, // 4 bits
    0b00011111, // 5 bits
    0b00111111, // 6 bits
    0b01111111, // 7 bits
    0b11111111, // 8 bits
  };


  //! Defines a mask to keep most significant bits of a nibble
  //!
  constexpr uint8_t LEFT_BITS_MASK_4[] =
  {
    0b0000, // 0 bits
    0b1000, // 1 bits
    0b1100, // 2 bits
    0b1110, // 3 bits
    0b1111, // 4 bits
  };

  constexpr std::array<uint8_t, 8> BIT_MASK_8 =
  {
    0b10000000,  // 00
    0b01000000,  // 01
    0b00100000,  // 02
    0b00010000,  // 03
    0b00001000,  // 04
    0b00000100,  // 05
    0b00000010,  // 06
    0b00000001,  // 07
  };

  static const std::array<string_view, 16> BINARY_NIBBLES =
  {
    "0000",  // 00
    "0001",  // 01
    "0010",  // 02
    "0011",  // 03
    "0100",  // 04
    "0101",  // 05
    "0110",  // 06
    "0111",  // 07
    "1000",  // 08
    "1001",  // 09
    "1010",  // 10
    "1011",  // 11
    "1100",  // 12
    "1101",  // 13
    "1110",  // 14
    "1111",  // 15
  };

  constexpr std::array<uint64_t, 65> MAX_UNSIGNED_VALUE_FOR_BITS_COUNT =
  {
    0x0000000000000000, // 00 bits
    0x0000000000000001, // 01 bits
    0x0000000000000003, // 02 bits
    0x0000000000000007, // 03 bits
    0x000000000000000f, // 04 bits
    0x000000000000001f, // 05 bits
    0x000000000000003f, // 06 bits
    0x000000000000007f, // 07 bits
    0x00000000000000ff, // 08 bits
    0x00000000000001ff, // 09 bits
    0x00000000000003ff, // 10 bits
    0x00000000000007ff, // 11 bits
    0x0000000000000fff, // 12 bits
    0x0000000000001fff, // 13 bits
    0x0000000000003fff, // 14 bits
    0x0000000000007fff, // 15 bits
    0x000000000000ffff, // 16 bits
    0x000000000001ffff, // 17 bits
    0x000000000003ffff, // 18 bits
    0x000000000007ffff, // 19 bits
    0x00000000000fffff, // 20 bits
    0x00000000001fffff, // 21 bits
    0x00000000003fffff, // 22 bits
    0x00000000007fffff, // 23 bits
    0x0000000000ffffff, // 24 bits
    0x0000000001ffffff, // 25 bits
    0x0000000003ffffff, // 26 bits
    0x0000000007ffffff, // 27 bits
    0x000000000fffffff, // 28 bits
    0x000000001fffffff, // 29 bits
    0x000000003fffffff, // 30 bits
    0x000000007fffffff, // 31 bits
    0x00000000ffffffff, // 32 bits
    0x00000001ffffffff, // 33 bits
    0x00000003ffffffff, // 34 bits
    0x00000007ffffffff, // 35 bits
    0x0000000fffffffff, // 36 bits
    0x0000001fffffffff, // 37 bits
    0x0000003fffffffff, // 38 bits
    0x0000007fffffffff, // 39 bits
    0x000000ffffffffff, // 40 bits
    0x000001ffffffffff, // 41 bits
    0x000003ffffffffff, // 42 bits
    0x000007ffffffffff, // 43 bits
    0x00000fffffffffff, // 44 bits
    0x00001fffffffffff, // 45 bits
    0x00003fffffffffff, // 46 bits
    0x00007fffffffffff, // 47 bits
    0x0000ffffffffffff, // 48 bits
    0x0001ffffffffffff, // 49 bits
    0x0003ffffffffffff, // 50 bits
    0x0007ffffffffffff, // 51 bits
    0x000fffffffffffff, // 52 bits
    0x001fffffffffffff, // 53 bits
    0x003fffffffffffff, // 54 bits
    0x007fffffffffffff, // 55 bits
    0x00ffffffffffffff, // 56 bits
    0x01ffffffffffffff, // 57 bits
    0x03ffffffffffffff, // 58 bits
    0x07ffffffffffffff, // 59 bits
    0x0fffffffffffffff, // 60 bits
    0x1fffffffffffffff, // 61 bits
    0x3fffffffffffffff, // 62 bits
    0x7fffffffffffffff, // 63 bits
    0xffffffffffffffff, // 64 bits
  };

} // End of unnamed namespace



//! Initializes with constant value for all bits
//!
//! @param bitsCount      Number of bits of resulting BinaryVector
//! @param fillPattern    Filling patern (may be truncated at the end)
//! @param sizeProperty   Tells whether BinaryVector size (number of bits) can be changed once constructed
//!
BinaryVector::BinaryVector (uint32_t bitsCount, uint8_t fillPattern, SizeProperty sizeProperty)
  : m_data         ((bitsCount + 7) / 8, fillPattern)
  , m_usedBits     (bitsCount)
  , m_sizeProperty (sizeProperty)
{
  auto lastByteBitsCount = bitsCount % 8;
  if (lastByteBitsCount != 0)
  {
    m_data.back() &= LEFT_BITS_MASK_8[lastByteBitsCount];
  }
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------



//! Copy constructor
//!
BinaryVector::BinaryVector (const mast::BinaryVector& rhs)
  : m_data         (rhs.m_data)
  , m_usedBits     (rhs.m_usedBits)
  , m_sizeProperty (rhs.m_sizeProperty == SizeProperty::FixedOnCopy ? SizeProperty::FixedOnCopy : SizeProperty::NotFixed)
{
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Move constructor
//!
BinaryVector::BinaryVector (mast::BinaryVector&& rhs) noexcept
  : m_data         (std::move(rhs.m_data))
  , m_usedBits     (rhs.m_usedBits)
  , m_sizeProperty (rhs.m_sizeProperty == SizeProperty::FixedOnCopy ? SizeProperty::FixedOnCopy : SizeProperty::NotFixed)
{
  rhs.m_usedBits     = 0;
  rhs.m_sizeProperty = SizeProperty::NotFixed;
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Constructs from raw data
//!
//! @note Raw data must be formatted as:
//!         - MSB comes first (data[0])
//!         - For last byte, bits are left aligned (on most significant bits)
//!
//! @note If data does not contained exactly the number of bytes for specified bits count,
//!       the content is either truncated or extended with zeros
//!
//! @param data           Raw data (formated as expected)
//! @param bitsCount      Number of effectively used bits
//! @param sizeProperty   Size property
//!
BinaryVector::BinaryVector (const std::vector<uint8_t>& data, uint32_t bitsCount, SizeProperty sizeProperty)
  : m_data         (data)
  , m_usedBits     (bitsCount)
  , m_sizeProperty (sizeProperty)
{
  auto expectedSize = (bitsCount + 7u) / 8u;
  m_data.resize(expectedSize);
  MaskLastByte();
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Constructs from "moveable" raw data
//!
//! @note Raw data must be formatted as:
//!         - MSB comes first (data[0])
//!         - For last byte, bits are left aligned (on most significant bits)
//!
//! @note If data does not contained exactly the number of bytes for specified bits count,
//!       the content is either truncated or extended with zeros
//!
//! @param data           Raw data (formated as expected)
//! @param bitsCount      Number of effectively used bits
//! @param sizeProperty   Size property
//!
BinaryVector::BinaryVector (std::vector<uint8_t>&& data, uint32_t bitsCount, SizeProperty sizeProperty)
  : m_data         (std::move(data))
  , m_usedBits     (bitsCount)
  , m_sizeProperty (sizeProperty)
{
  auto expectedSize = (bitsCount + 7u) / 8u;
  m_data.resize(expectedSize);
  MaskLastByte();
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Appends another scan vector
//!
BinaryVector& BinaryVector::Append (const BinaryVector& rhs)
{
  CHECK_FIXED_SIZE;

  uint32_t       bitsToAppend = rhs.m_usedBits;
  const uint8_t* pRhsData     = rhs.m_data.data();

  while (bitsToAppend >= 8)
  {
    bitsToAppend -= 8;
    Append(*pRhsData++);
  }

  if (bitsToAppend != 0)
  {
    auto lastValue = *pRhsData;
    Append(lastValue, bitsToAppend, BitsAlignment::Left);
  }

  return *this;
}
//
//  End of: BinaryVector::Append
//---------------------------------------------------------------------------



//! Appends from 8 bits
//!
//! @param value          The value to append to the vector
//! @param numberOfBits   Number of useful bits in the value
//! @param alignment      Tells whether bits are left (msb) or right (lsb) aligned
//!
BinaryVector& BinaryVector::Append (uint8_t value, uint8_t numberOfBits, BitsAlignment alignment)
{
  if (numberOfBits == 0)
  {
    THROW_INVALID_ARGUMENT("Number of bits to append must be != 0");
  }

  CHECK_FIXED_SIZE;

  if (numberOfBits > 8 * sizeof(uint8_t))
  {
    THROW_INVALID_ARGUMENT("Number of bits to append cannot exceed the number of bits of value");
  }

  // ---------------- Align (pack) added bits to the MSB
  //                  This make sure that unused bits are set to zero (at least for test and debug purpose)
  //
  if (alignment == BitsAlignment::Right)
  {
    value <<= 8 - numberOfBits;
  }
  value &=  LEFT_BITS_MASK_8[numberOfBits];

  const uint8_t lastByteBits = m_usedBits % 8;
  const uint8_t freeBits     = (lastByteBits == 0) ? 8 : 8 - lastByteBits;

  if (freeBits == 8)
  {
    m_data.push_back(value);    // Value is already aligned on MSB
  }
  else if (freeBits >= numberOfBits)
  {
    auto lastByte     = m_data.back();
    auto shiftCount   = 8 - freeBits;
    auto shiftedValue = value    >> shiftCount;
    auto newByte      = lastByte |  shiftedValue;

    m_data.back()     = newByte;
  }
  else
  { // Added value must be split into 2 bytes

    // ---------------- First part (mixed with previous last value)
    //
    uint8_t lastByte       = m_data.back();
    uint8_t shiftedValue_1 = value    >> lastByteBits;
    uint8_t newByte_1      = lastByte |  shiftedValue_1;

    m_data.back()       = newByte_1;

    // ---------------- Second part
    //
    uint8_t newByte_2 = value << freeBits;  // Free bits have been used for first part of value

    m_data.push_back(newByte_2);
  }

  m_usedBits += numberOfBits;

  return *this;
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Appends from 16 bits
//!
//! @param value          The value to append to the vector
//! @param numberOfBits   Number of useful bits in the value
//! @param alignment      Tells whether bits are left (msb) or right (lsb) aligned
//!
BinaryVector& BinaryVector::Append (uint16_t value, uint8_t numberOfBits, BitsAlignment alignment)
{
  CHECK_FIXED_SIZE;
  CHECK_AT_LEAST_1_BIT(numberOfBits);

  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    uint8_t byte_1 = asBytes[1];
    uint8_t byte_2 = asBytes[0];
  #else
    uint8_t byte_1 = asBytes[0];
    uint8_t byte_2 = asBytes[1];
  #endif

  auto chunksList = {byte_1, byte_2};
  return AppendChunks(numberOfBits, alignment, chunksList);
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------



//! Appends from 32 bits
//!
//! @param value          The value to append to the vector
//! @param numberOfBits   Number of useful bits in the value
//! @param alignment      Tells whether bits are left (msb) or right (lsb) aligned
//!
BinaryVector& BinaryVector::Append (uint32_t value, uint8_t numberOfBits, BitsAlignment alignment)
{
  CHECK_FIXED_SIZE;
  CHECK_AT_LEAST_1_BIT(numberOfBits);

  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    uint8_t byte_1 = asBytes[3];
    uint8_t byte_2 = asBytes[2];
    uint8_t byte_3 = asBytes[1];
    uint8_t byte_4 = asBytes[0];
  #else
    uint8_t byte_1 = asBytes[0];
    uint8_t byte_2 = asBytes[1];
    uint8_t byte_3 = asBytes[2];
    uint8_t byte_4 = asBytes[3];
  #endif

  auto chunksList = {byte_1, byte_2, byte_3, byte_4};
  return AppendChunks(numberOfBits, alignment, chunksList);
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Appends from 64 bits
//!
//! @param value          The value to append to the vector
//! @param numberOfBits   Number of useful bits in the value
//! @param alignment      Tells whether bits are left (msb) or right (lsb) aligned
//!
BinaryVector& BinaryVector::Append (uint64_t value, uint8_t numberOfBits, BitsAlignment alignment)
{
  CHECK_FIXED_SIZE;
  CHECK_AT_LEAST_1_BIT(numberOfBits);

  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    uint8_t byte_1 = asBytes[7];
    uint8_t byte_2 = asBytes[6];
    uint8_t byte_3 = asBytes[5];
    uint8_t byte_4 = asBytes[4];
    uint8_t byte_5 = asBytes[3];
    uint8_t byte_6 = asBytes[2];
    uint8_t byte_7 = asBytes[1];
    uint8_t byte_8 = asBytes[0];
  #else
    uint8_t byte_1 = asBytes[0];
    uint8_t byte_2 = asBytes[1];
    uint8_t byte_3 = asBytes[2];
    uint8_t byte_4 = asBytes[3];
    uint8_t byte_5 = asBytes[4];
    uint8_t byte_6 = asBytes[5];
    uint8_t byte_7 = asBytes[6];
    uint8_t byte_8 = asBytes[7];
  #endif

  auto chunksList = {byte_1, byte_2, byte_3, byte_4, byte_5, byte_6, byte_7, byte_8};

  return AppendChunks(numberOfBits, alignment, chunksList);
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Appends 0 or 1 bit N times
//!
//! @param bitIsOne   When true '1' otherwise '0' are appended to the vector
//! @param count      Number of '0' or '1' to append
//!
BinaryVector& BinaryVector::AppendBits (bool bitIsOne, uint32_t count)
{
  uint8_t value = bitIsOne ? 0xFF : 0;
  auto    bytesCount = count / 8u;

  if (bytesCount != 0)
  {
    m_data.insert(m_data.cend(), bytesCount, value);

    auto addedBits = 8u * bytesCount;
    m_usedBits    += addedBits;
    count         -= addedBits;
  }

  if (count != 0)
  {
    Append(value, count);
  }

  return *this;
}
//
//  End of: BinaryVector::AppendBits
//---------------------------------------------------------------------------


//! Appends from a list of uint8_t ordered from msb to lsb
//!
//! @param numberOfBits   Number of bits to use from chunksList
//! @param alignment      Tells whether (source) bits in chunksList are left (msb) or right (lsb) aligned
//! @param chunksList     A set of uint8_t from msb to lsb
//!
BinaryVector& BinaryVector::AppendChunks (uint8_t numberOfBits, BitsAlignment alignment, initializer_list<uint8_t> chunksList)
{
  CHECK_FIXED_SIZE;
  CHECK_AT_LEAST_1_BIT(numberOfBits);

  uint8_t maxBits   = chunksList.size() * 8u;
  if (numberOfBits > maxBits)
  {
    THROW_INVALID_ARGUMENT("Numbers of bits cannot exceed bits count of chunks list");
  }

  uint8_t threshold = maxBits - 8u;
  auto    chunks    = chunksList.begin();
  uint8_t chunkId   = 0;

  // ---------------- Assess which byte to start from and threshold value to know when to stop processing
  //
  while (threshold >= numberOfBits)
  {
    threshold -= 8;
    if (alignment == BitsAlignment::Right)
    {
      ++chunkId;
    }
  }

  while (numberOfBits > threshold)
  {
    uint8_t bits              = chunks[chunkId];
    uint8_t maxBits           = std::min(numberOfBits, static_cast<uint8_t>(8u));
    uint8_t chunkNumberOfBits = (alignment == BitsAlignment::Left) ? maxBits
                                                                   : numberOfBits - threshold;

    Append(bits, chunkNumberOfBits, alignment);
    numberOfBits -= chunkNumberOfBits;

    if (threshold == 0)
    {
      break;
    }
    threshold -= 8u;
    ++chunkId;
  }
  return *this;
}
//
//  End of: BinaryVector::AppendChunks
//---------------------------------------------------------------------------


//! Clears all content
//!
//! @note Post-condition is that bits and bytes count are zeros
//!
void BinaryVector::Clear ()
{
  m_data.clear();
  m_usedBits = 0;
}
//
//  End of: BinaryVector::Clear
//---------------------------------------------------------------------------


//! Clears specified bit (to zero)
//!
//! @param bitOffset Zero based bit offset (from left)
//!
void BinaryVector::ClearBit (uint32_t bitOffset)
{
  CHECK_PARAMETER_LT(bitOffset, m_usedBits, "Out of range bit id: "s + to_string(bitOffset));

  auto byteOffset      = bitOffset / 8;
  auto bitOffsetInByte = bitOffset % 8;

  m_data[byteOffset] &= ~BIT_MASK_8[bitOffsetInByte];
}
//
//  End of: BinaryVector::ClearBit
//---------------------------------------------------------------------------


//! Returns true when *this is equal to another BinaryVector
//!
//! @note Fixed size property is not compared (only the value)
//!
bool BinaryVector::CompareEqualTo (const BinaryVector& rhs) const
{
  if (m_usedBits != rhs.m_usedBits)
  {
    return false;
  }

  if (m_usedBits == 0)
  {
    return true;
  }

  auto bitsOnLastByte = m_usedBits % 8;
  auto areEqual       = true;

  if (bitsOnLastByte == 0)
  {
    areEqual = m_data == rhs.m_data;
  }
  else
  {
    if (m_data.size() > 1)
    {
      areEqual = std::equal(m_data.cbegin(), m_data.cend() - 1, rhs.m_data.cbegin(), rhs.m_data.cend() - 1);
    }

    if (areEqual)
    {
      auto left  = m_data.back()     & LEFT_BITS_MASK_8[bitsOnLastByte];
      auto right = rhs.m_data.back() & LEFT_BITS_MASK_8[bitsOnLastByte];

      areEqual = left == right;
    }
  }

  return areEqual;
}
//
//  End of: BinaryVector::CompareEqualTo
//---------------------------------------------------------------------------



//! Returns true when *this is equal to another BinaryVector taking account a don't care mask
//!
//! @note fall back to standard CompareEqualTo when the mask is empty (to simplify usage)
//! @note Fixed size property is not compared (only the value)
//!
//! @internal
//! @note Note used bits are supposed to be forced to zero!
//! @note It is optimized for cases both vectors are most often equal (at least the first bytes)
//! @endinternal
//!
//! @param rhs            A BinaryVector to compare to
//! @param dontCareMask   A mask for bits to compare (1s) and ignore (0s)
//!
//! @return true When all __cared-for bits__ are equal, false otherwise
//!
bool BinaryVector::CompareEqualTo (const BinaryVector& rhs, const BinaryVector& dontCareMask) const
{
  if (dontCareMask.IsEmpty())
  {
    return CompareEqualTo(rhs);
  }

  if (m_usedBits != rhs.m_usedBits)
  {
    return false;
  }

  if (m_usedBits == 0)
  {
    return true;
  }

  CHECK_SAME_SIZE(dontCareMask);


  auto pLhs    = m_data.cbegin();
  auto pLhsEnd = m_data.cend();
  auto pRhs    = rhs.m_data.cbegin();
  auto pMask   = dontCareMask.m_data.cbegin();

  while (pLhs != pLhsEnd)
  {
    auto lhsByte = *pLhs++;
    auto rhsByte = *pRhs++;

    if (lhsByte != rhsByte)
    {
      auto mask          = *pMask;
      auto compareResult = (lhsByte ^ rhsByte) & mask;

      if (compareResult != 0)
      {
        return false;
      }
    }

    ++pMask;
  }

  return true;
}
//
//  End of: BinaryVector::CompareEqualTo
//---------------------------------------------------------------------------


//! Creates a BinaryVector from text binary representation
//!
//! @note Firstly intended for test purposes, but can be used for anything else
//!
//! @param bits         Sequence of characters representing content of BinaryVector to create
//!                     Characters in \",':_- \\t/\|\" are ignored (can be used to ease display of string)
//!                     An exception is thrown if there is any character different from
//!                     set \"01,':_- \\t\\n\"
//!                     '0b' is ignored at start of string. An exception is thrown everywhere else
//!                     '/b', '/B', '\b', '\B' constructions are ignored anywhere
//! @param sizeProperty Size property
//! @param dontCare     Tells how to handle "dont't care" special characters 'x' and 'X'
//!
//! @return A new BinaryVector initialized as defined by bits text
#include <iostream>
BinaryVector BinaryVector::CreateFromBinaryString (std::experimental::string_view bits, SizeProperty sizeProperty, DontCare dontCare)
{
  CHECK_PARAMETER_NOT_NULL(bits.data(), "BinaryVector cannot be created from nullptr");
  BinaryVector result;

  uint8_t nextByte = 0;
  auto    bitCount = 0;


  // ---------------- Skip leading blank chars
  //
  auto bitId  = size_t(0);
  while (   (bitId < bits.length())
         && (   (bits[bitId] == '\n')
             || (bits[bitId] == '\t')
             || (bits[bitId] == ' ')
            )
        )
  {
    ++bitId;
  }
  bits.remove_prefix(bitId);

  // ---------------- Tolerate strings beginning with "0b"
  //
  if (bits.length() >= 2
      && ((bits[1] == 'b') || (bits[1] == 'B'))
      &&  (bits[0] == '0')
     )
  {
    bits.remove_prefix(2);
  }

  auto appendBit = [&](uint8_t bit)   // Helper to append one bit (zero or one)
  {
    nextByte <<= 1;
    nextByte |= bit;

    if (++bitCount == 8)
    {
      result.m_data.push_back(nextByte);
      result.m_usedBits += 8;

      nextByte = 0;
      bitCount = 0;
    }
  };

  string_view::value_type previousChar = '\0'; // To detect construction like '\b' and '/b' that are tolerated
  for (const auto& nextChar : bits)
  {
    switch (nextChar)
    {
      case '0':
        appendBit(0);
        break;
      case '1':
        appendBit(1);
        break;
      case 'x':
      case 'X':
      {
        switch (dontCare)
        {
          case DontCare::IsError:
            THROW_INVALID_ARGUMENT("CreateFromBinaryString found unexpected \"don't care character\"");
            break;
          case DontCare::IsZero:
            appendBit(0);
            break;
          case DontCare::IsOne:
            appendBit(1);
            break;
          default:
            CHECK_TRUE(false, "Unexpected 'dontCare' value");
            break;
        }
        break;
      }
      case '-':
      case '_':
      case ':':
      case '|':
      case ',':
      case '\'':
      case '\t':
      case '\n':
      case ' ':
      case '\0':
      case '/':
      case '\\':
        break;  // Ignored characters
      case 'b':
      case 'B':
        if ((previousChar != '\\') && (previousChar != '/'))
        {
          THROW_INVALID_ARGUMENT("CreateFromBinaryString support only constructions: '\\b' and '/b'");
        }
        break;
      default:
        THROW_INVALID_ARGUMENT("CreateFromBinaryString only support characters in '01,\':|_-\\x20\\t\\n'");
        break;
    }
    previousChar = nextChar;
  }

  // ---------------- Append remaining bits (when not a multiple of 8 bits)
  //
  if (bitCount != 0)
  {
    nextByte <<= 8 - bitCount;
    result.m_data.push_back(nextByte);
    result.m_usedBits += bitCount;
  }

  result.m_sizeProperty = sizeProperty;
  return result;
}
//
//  End of: BinaryVector::CreateFromBinaryString
//---------------------------------------------------------------------------


//! Creates a BinaryVector from text decimal representation
//!
//! @note It only support values that can be encoded using uint64_t (18446744073709551615)
//!
//! @param bits         Sequence of characters representing content of BinaryVector to create:
//!                     Characters in \",':_- \t/\" are ignored (can be used to ease display of string)
//!                     An exception is thrown if there is any character different from
//!                     set \"0123456789,':_- \\t\\n/\"
//!                     'd is ignored at start of string (ICL format). An exception is thrown everywhere else.
//!                     Don't care 'x' or 'X' are not allowed and lead to thrown exception
//!
//! @param sizeProperty Size property
//! @param dontCare     Tells how to handle "dont't care" special characters 'x' and 'X'
//!
//! @return A new BinaryVector initialized as defined by bits text
//!
BinaryVector BinaryVector::CreateFromDecString (string_view bits, SizeProperty sizeProperty)
{
  CHECK_PARAMETER_NOT_NULL(bits.data(), "BinaryVector cannot be created from nullptr");

  // ---------------- Skip leading blank spaces
  //
  Utility::TrimLeft(bits);

  // ---------------- Tolerate empty vector
  //
  if (bits.empty())
  {
    return BinaryVector();
  }

  // ---------------- Capture "inverted bits" option
  //
  auto inverted = false;
  if (bits[0] == '~')
  {
    inverted = true;
    bits.remove_prefix(1);
  }

  auto bitsCount    = 32u;  // This is the default integer size
  auto parsedDigits = false;
  auto hasSize      = false;

  // ---------------- Skip leading zeros
  //
  while (bits.front() == '0')
  {
    parsedDigits = true;
    bits.remove_prefix(1);
  }

  auto     nextMustBe_D = false; // Memorize that we parsed a quote that must be followed with a 'd' or 'D'
  uint64_t value        = 0;

  for (const auto nextChar : bits)
  {
    CHECK_PARAMETER_TRUE(!nextMustBe_D || (nextChar == 'd') || (nextChar == 'D'), "Unexpected character '"s.append(1, nextChar).append("'after quote (') found while parsing decimal number: " + bits));

    auto     hasDigit = true;
    uint64_t digit    = 0;

    switch (nextChar)
    {
      case '0': digit = 0; break;
      case '1': digit = 1; break;
      case '2': digit = 2; break;
      case '3': digit = 3; break;
      case '4': digit = 4; break;
      case '5': digit = 5; break;
      case '6': digit = 6; break;
      case '7': digit = 7; break;
      case '8': digit = 8; break;
      case '9': digit = 9; break;
      case '\'':
      {
        CHECK_PARAMETER_TRUE(!hasSize, "Unexpected quote (') found while parsing decimal number: " + bits);
        if (parsedDigits)
        {
          bitsCount    = value;
          value        = 0;
          CHECK_PARAMETER_NOT_ZERO(bitsCount, "Bits count must be at least one!");
          parsedDigits = false;
          hasSize      = true;
        }
        nextMustBe_D = true;
        hasDigit     = false;
        break;
      }
      case 'd':
      case 'D':
        CHECK_PARAMETER_TRUE(nextMustBe_D, "Unexpected 'd' found while parsing decimal number: " + bits);
        nextMustBe_D = false;
        break;  // Condition has been checked at top of loop, we can ignore it safely
      case '-':
      case '_':
      case ':':
      case '|':
      case ',':
      case '\t':
      case '\n':
      case ' ':
      case '\0':
      case '/':
      case '\\':
        hasDigit = false;
        break;  // Ignored characters
      case 'x':
      case 'X':
        THROW_INVALID_ARGUMENT("CreateFromDecString found unexpected \"don't care character\" that is not supported in decimal values");
      default:
        THROW_INVALID_ARGUMENT("CreateFromDecString only support characters in '0123456789xX,\':|_-\\x20\\t\\n/', got: '"s.append(1, nextChar).append("'"));
    }

    if (hasDigit)
    {
      parsedDigits = true;
      if (value > (UINT64_MAX / 10u))
      {
        THROW_OUT_OF_RANGE("Parsed value is out of uint64_t range");
      }

      value *= 10u;

      if ((value + digit) < value)
      {
        THROW_OUT_OF_RANGE("Parsed value is out of uint64_t range");
      }
      value += digit;
    }
  }

  CHECK_PARAMETER_TRUE(parsedDigits, "Failed to parse an integer from: "s + bits);

  // ---------------- Fix bit count?
  //
  if (!hasSize && (value > UINT32_MAX))
  {
    bitsCount = 64u;
  }

  BinaryVector result;
  result.Append(value, bitsCount, BitsAlignment::Right);
  result.m_sizeProperty = sizeProperty;
  if (inverted)
  {
    result.ToggleBits();
  }

  return result;
}
//
//  End of: BinaryVector::CreateFromBinaryString
//---------------------------------------------------------------------------



//! Creates a BinaryVector from text hexadecimal representation
//!
//! @note Firstly intended for test purposes, but can be used for anything else
//!
//! @param bits         Sequence of characters representing content of BinaryVector to create
//!                     Characters in \",':_- \t/\" are ignored (can be used to ease display of string)
//!                     An exception is thrown if there is any character different from
//!                     set \"0123456789abcdefABCDEF,':_- \\t\\n/\"
//!                     '0x' is ignored at start of string. An exception is thrown everywhere else
//!                     '/x', '/X', '\x', '\X' constructions are ignored anywhere
//! @param sizeProperty Size property
//! @param dontCare     Tells how to handle "dont't care" special characters 'x' and 'X'
//!
//! @return A new BinaryVector initialized as defined by bits text
//!
BinaryVector BinaryVector::CreateFromHexString (string_view bits, SizeProperty sizeProperty, DontCare dontCare)
{
  CHECK_PARAMETER_NOT_NULL(bits.data(), "BinaryVector cannot be created from nullptr");
  BinaryVector result;

  uint8_t nextByte = 0;
  auto    bitCount = 0;

  // ---------------- Skip leading blank chars
  //
  auto bitId  = size_t(0);
  while (   (bits[bitId] == '\n')
         || (bits[bitId] == '\t')
         || (bits[bitId] == ' ')
        )
  {
    ++bitId;
  }
  bits.remove_prefix(bitId);

  // ---------------- Tolerate strings beginning with "0x"
  //
  if (bits.length() >= 2
      && ((bits[1] == 'x') || (bits[1] == 'X'))
      &&  (bits[0] == '0')
     )
  {
    bits.remove_prefix(2);
  }

  string_view::value_type previousChar = '\0'; // To detect construction like '\x' and '/x' that are tolerated
  for (const auto& nextChar : bits)
  {
    auto    hasValue = true;
    uint8_t value    = 0;

    switch (nextChar)
    {
      case '0': value = 0x0; break;
      case '1': value = 0x1; break;
      case '2': value = 0x2; break;
      case '3': value = 0x3; break;
      case '4': value = 0x4; break;
      case '5': value = 0x5; break;
      case '6': value = 0x6; break;
      case '7': value = 0x7; break;
      case '8': value = 0x8; break;
      case '9': value = 0x9; break;
      case 'A':
      case 'a': value = 0xa; break;
      case 'B':
      case 'b': value = 0xb; break;
      case 'C':
      case 'c': value = 0xc; break;
      case 'D':
      case 'd': value = 0xd; break;
      case 'E':
      case 'e': value = 0xe; break;
      case 'F':
      case 'f': value = 0xf; break;
      case '-':
      case '_':
      case ':':
      case '|':
      case ',':
      case '\'':
      case '\t':
      case '\n':
      case ' ':
      case '\0':
      case '/':
      case '\\':
        hasValue = false;
        break;  // Ignored characters
      case 'x':
      case 'X':
        if ((previousChar == '\\') || (previousChar == '/'))
        {
          hasValue = false;
        }
        else
        {
          switch (dontCare)
          {
            case DontCare::IsError: THROW_INVALID_ARGUMENT("CreateFromHexString found unexpected \"don't care character\"");
            case DontCare::IsZero: value = 0x0; break;
            case DontCare::IsOne:  value = 0xF; break;
            default: CHECK_TRUE(false, "Unexpected 'dontCare' value"); break;
          }
          break;
        }
        break;
      default:
        THROW_INVALID_ARGUMENT("CreateFromHexString only support characters in '01,\':|_-\\x20\\t\\n/\\'");
    }

    if (hasValue)
    {
      nextByte |= value;
      bitCount += 4;
      if (bitCount == 8)
      {
        result.m_data.push_back(nextByte);
        result.m_usedBits += 8;

        nextByte = 0;
        bitCount = 0;
      }
      else
      {
        nextByte <<= 4;
      }
    }

    previousChar = nextChar;
  }

  // ---------------- Append remaining bits (when not a multiple of 8 bits)
  //
  if (bitCount != 0)
  {
    result.m_data.push_back(nextByte);
    result.m_usedBits += bitCount;
  }

  result.m_sizeProperty = sizeProperty;

  return result;
}
//
//  End of: BinaryVector::CreateFromBinaryString
//---------------------------------------------------------------------------


//! Creates a BinaryVector from mix of hexadecimal and binary representation
//!
//! @note Support both SIT and ICL formats
//!       For ICL see IEEE P1687/D1.71 �6.3.11 "Inversion and concatenation"
//! @note When using ICL format:
//!       - Sized numbers may be concatenated with other sized numbers, and at most one unsized number
//!         can be included with a group of sized numbers.
//!         Since the size of all targets is known, the "size" of an unsized number in this situation is
//!         inferred to be the difference between the target size and the sum of the sized numbers�
//!         widths and is required to be equal to or greater than the number of bits required for the
//!         unsized number.
//!       - When a value is preceded by a tilde (~), each bit in the binary representation of the value
//!         of that identifier is complemented.
//!
//! @note Supports empty vector only using empty (or spaces only) input string
//!
//! @param bits         Sequence of characters representing content of BinaryVector to create
//!                     Characters in \",':_- \t/\" are ignored (can be used to ease display of string)
//!                     An exception is thrown if there is any character different from
//!                     set \"0123456789abcdefABCDEF,':_- \t/\"
//!                     '0x' is ignored at start of string. An exception is thrown everywhere else
//!                     '/x', '/X', '\\x', '\\X' ''h', ''H' constructions are interpreted as: What follow is hexadecimal
//!                     '/b', '/B', '\\b', '\\B' ''b', ''B' constructions are interpreted as: What follow is binary
//!
//! @param sizeProperty Size property tell whether the size can change after construction and if this property is copied to destination
//! @param dontCare     Tells how to handle "dont't care" special characters 'x' and 'X'
//!
//! @return A new BinaryVector initialized as defined by bits text
//!
BinaryVector BinaryVector::CreateFromString (string_view stringValue, SizeProperty sizeProperty, DontCare dontCare)
{
  CHECK_PARAMETER_NOT_NULL(stringValue.data(), "BinaryVector cannot be created from nullptr");

  // ---------------- Skip leading blank spaces
  //
  Utility::TrimLeft(stringValue);

  // ---------------- Tolerate empty vector
  //
  if (stringValue.length() == 0)
  {
    return BinaryVector();
  }

  auto firstChunk = true;

  // Lamba: Extract base, optional stringValue inversion and stringValue count from leading sequence of characters (can be embedded in a large list of sequences)
  //
  auto extractEncodingInfo = [&firstChunk](string_view stringValue)
  {
    auto firstChar   = stringValue.front();
    auto isStdPrefix = firstChunk ? Utility::Contains("0/\\", firstChar)
                                  : Utility::Contains("/\\",  firstChar);
    firstChunk = false;

    if (isStdPrefix)
    {
      auto format = NumberBaseForValuePrefix(stringValue.substr(1u));
      if (format != NumberBase::Undefined)
      {
        stringValue.remove_prefix(2u); // Remove '0x' or /x or \x ...
      }
      return make_tuple(EncodingInfo(format), stringValue);
    }

    // ICL syntax?
    EncodingInfo encodingInfo;

    if (firstChar == '~')
    {
      stringValue.remove_prefix(1u);
      encodingInfo.inverted = true;
      if (stringValue.empty())
      {
        return make_tuple(encodingInfo, stringValue);
      }
    }

    size_t bitId = 0;

    if (Utility::Contains("123456789", stringValue.front()))  // length prefix
    {
      tie(encodingInfo.bitsCount, bitId) = Utility::ToUInt32(stringValue);
    }

    if ((bitId < stringValue.length()) && (stringValue[bitId] == '\''))
    {
      stringValue.remove_prefix(bitId + 1u);

      encodingInfo.base = NumberBaseForValuePrefix(stringValue);
      if (encodingInfo.base != NumberBase::Undefined)
      {
        stringValue.remove_prefix(1u);
      }
    }
    else if (encodingInfo.bitsCount != 0)
    {
      encodingInfo.bitsCount = 0;
      encodingInfo.base      = NumberBase::Decimal;
    }
    return make_tuple(encodingInfo, stringValue);
  };

  // Lamba: Provides next chunk of string encode value to parse along with its encoding infor
  //
  auto getNextChunk = [&extractEncodingInfo](string_view stringValue)
  {
    Utility::TrimLeft(stringValue);

    EncodingInfo encodingInfo;
    std::tie(encodingInfo, stringValue) = extractEncodingInfo(stringValue);

    auto chunk = stringValue;  // Default: all remaining is supposed to be of single base encoding

    if (encodingInfo.base == NumberBase::Undefined)
    {
      return make_tuple(encodingInfo, chunk, stringValue);
    }


    // ---------------- Find other format indication
    //
    auto offset = stringValue.find_first_of("/,\\'", 0u);
    if (   (offset == string_view::npos)  // Not found
        || (offset == stringValue.length())      // Found at end of string ==> can be ignored
       )
    {
      stringValue.remove_prefix(stringValue.length());       // Nothing to process after this chunk
    }
    else
    {
      chunk = stringValue.substr(0, offset);   // Take only first part
      stringValue.remove_prefix(offset);       // Remove first part from remaining
      if (stringValue.front() == ',')
      {
        stringValue.remove_prefix(1);
        Utility::TrimLeft(stringValue);
      }
    }

    return make_tuple(encodingInfo, chunk, stringValue);
  };

  // ---------------- Core job starts here
  //

  // Skip leading blank chars
  //
  while (!stringValue.empty() && Utility::Contains(" \t\n", stringValue.front()))
  {
    stringValue.remove_prefix(1u);
  }

  BinaryVector result;
  BinaryVector chunkVector;

  while (!stringValue.empty())
  {
    string_view  bitsChunk;
    EncodingInfo encodingInfo;

    std::tie(encodingInfo, bitsChunk, stringValue) = getNextChunk(stringValue);

    CHECK_PARAMETER_NOT_EMPTY(bitsChunk, "Cannot parse empty number");

    auto bitsCount   = encodingInfo.bitsCount;
    auto hasSizeInfo = bitsCount != 0;
    switch (encodingInfo.base)
    {
      case NumberBase::Binary:
        chunkVector = BinaryVector::CreateFromBinaryString(bitsChunk, SizeProperty::NotFixed, dontCare);
        break;
      case NumberBase::Hexadecimal:
        chunkVector = BinaryVector::CreateFromHexString(bitsChunk, SizeProperty::NotFixed, dontCare);
        break;
      case NumberBase::Decimal:
        chunkVector = BinaryVector::CreateFromDecString(bitsChunk, SizeProperty::NotFixed);
        break;
      case NumberBase::Octal:
        THROW_INVALID_ARGUMENT("Octal numbers are not supported!");
        break;
      case NumberBase::Undefined:
        THROW_INVALID_ARGUMENT("Cannot tell whether value is in decimal, hexadecimal or binary");
      default:
        CHECK_FAILED("Houps, NumberBase not managed");
    }

    auto chunkBitsCount = chunkVector.BitsCount();

    if (hasSizeInfo && (bitsCount != chunkBitsCount))
    {
      if (bitsCount < chunkBitsCount)
      {
        LOG(WARNING) << "Truncating value: " << bitsChunk;
        chunkVector = chunkVector.Slice(chunkBitsCount - bitsCount, bitsCount); // Truncation
      }
      else
      {
        LOG(INFO) << "Extending value: " << bitsChunk;
        auto isNegative = chunkVector.IsNegative();
        auto appendOnes = encodingInfo.inverted ? !isNegative : isNegative;
        result.AppendBits(appendOnes, bitsCount - chunkBitsCount);
      }
    }

    if (encodingInfo.inverted)
    {
      chunkVector.ToggleBits();
    }
    result.Append(chunkVector);
  }

  result.m_sizeProperty = sizeProperty;
  return result;
}
//
//  End of: BinaryVector::CreateFromString
//---------------------------------------------------------------------------





//! Creates a BinaryVector from a "moveable" buffer with right aligned data (first byte is partially used and last one if fully used)
//!
//! @param buffer       Source buffer (with right aligned data)
//! @param bitsCount    Number of meaningful bits in source buffer
//! @param sizeProperty Size property
//!
BinaryVector BinaryVector::CreateFromRightAlignedBuffer (vector<uint8_t>&& buffer, uint32_t bitsCount, SizeProperty sizeProperty)
{
  ShiftBufferLeft(buffer, buffer, bitsCount);

  BinaryVector binaryVector(std::move(buffer), bitsCount, sizeProperty);
  return binaryVector;
}
//
//  End of: BinaryVector::CreateFromRightAlignedBuffer
//---------------------------------------------------------------------------


//! Creates a BinaryVector from a buffer with right aligned data (first byte is partially used and last one if fully used)
//!
//! @param buffer       Source buffer (with right aligned data)
//! @param bitsCount    Number of meaningful bits in source buffer
//! @param sizeProperty Size property
//!
BinaryVector BinaryVector::CreateFromRightAlignedBuffer (const vector<uint8_t>& buffer, uint32_t bitsCount, SizeProperty sizeProperty)
{
  vector<uint8_t> newBuffer(buffer.size());

  ShiftBufferLeft(buffer, newBuffer, bitsCount);

  BinaryVector binaryVector(std::move(newBuffer), bitsCount, sizeProperty);
  return binaryVector;
}


//! Gets content as formatted binary string
//!
//! @note An example of formatting is: 0001_1111:0011_0100:0101_010
//!
//! @param octoSeparator  Characters to insert every 8 bits
//! @param quadSeparator  Characters to insert every 4 bits
//! @param bytesPerLine   Number of bytes (sequence of 8 bits) to write per line.
//!                       When zero, all is on the "same line"
//! @param eolSeparator   Characters to insert just before new lines (when bytesPerLine != 0)
//! @param prefixWith0b   When true, "0b" will be prepended to the resulting string
//!
string BinaryVector::DataAsBinaryString (string_view quadSeparator,
                                         string_view octoSeparator,
                                         uint32_t    bytesPerLine,
                                         string_view eolSeparator,
                                         bool        prefixWith0b
                                        ) const
{

  ostringstream os;
  uint32_t      nibblesCount = 0;
  uint32_t      bytesCount   = 0;

  auto appendNibble = [&](string_view nibble)
  {
    if (nibblesCount == 1)
    {
      os << quadSeparator;
    }
    else if (nibblesCount == 2)
    {
      nibblesCount = 0;
      ++bytesCount;
      if (bytesCount == bytesPerLine)
      {
        os << eolSeparator << std::endl;
      }
      else
      {
        os << octoSeparator;
      }
    }

    ++nibblesCount;
    os << nibble;
  };

  auto appendBits = [&](uint8_t bitsOnLsb, uint32_t bitsCount)
  {
    if (bitsCount != 0)
    {
      auto nibble = BINARY_NIBBLES[bitsOnLsb];       // Get string representation with padded zero on the right
      nibble = string_view(nibble.data(), bitsCount);
      appendNibble(nibble);
    }
  };

  if (prefixWith0b)
  {
    os << "0b";
  }

  auto bitsCount = m_usedBits;

  for (auto byte : m_data)
  {
    if (bitsCount >= 8)
    {
      appendNibble(BINARY_NIBBLES[byte >> 4]);
      appendNibble(BINARY_NIBBLES[byte &  0x0F]);
      bitsCount -= 8;
    }
    else if (bitsCount >= 4)
    {
      appendNibble(BINARY_NIBBLES[byte >> 4]);
      bitsCount -= 4;
      appendBits(byte & 0x0F, bitsCount);
    }
    else
    {
      byte = (byte >> 4) & 0x0F; // Put bits on LSB
      appendBits(byte, bitsCount);
    }
  }

  return os.str();
}
//
//  End of: BinaryVector::DataAsBinaryString
//---------------------------------------------------------------------------


//! Gets content as formatted hexadecimal string (as saved internally - bits are appended from left to right)
//!
//! @note An example of formatting is: FACE_DEAD:BEEF_0123:CAFE_4 (where last '4' may mean '0b0100' or '0b010' or '0b01')
//! @note For precise display of last bits, please use DataAsMixString (or DataAsBinaryString)
//!
//! @param octoSeparator   Characters to insert every 32 bits
//! @param quadSeparator   Characters to insert every 16 bits
//! @param bytesPerLine    Number of bytes (sequence of 8 bits) to write per line.
//!                        When zero, all is on the "same line"
//! @param eolSeparator    Characters to insert just before new lines character (when bytesPerLine != 0)
//! @param prefixWith0x    When true, "0x" will be prepended to the resulting string
//!
string BinaryVector::DataAsHexString (string_view quadSeparator,
                                      string_view octoSeparator,
                                      uint32_t    bytesPerLine,
                                      string_view eolSeparator,
                                      bool        prefixWith0x
                                     ) const
{
  // ---------------- Associate 4 bits value with its hexadecimal representation
  //
  static const std::array<string_view, 16> nibbles =
  {
    "0",  // 00
    "1",  // 01
    "2",  // 02
    "3",  // 03
    "4",  // 04
    "5",  // 05
    "6",  // 06
    "7",  // 07
    "8",  // 08
    "9",  // 09
    "A",  // 10
    "B",  // 11
    "C",  // 12
    "D",  // 13
    "E",  // 14
    "F",  // 15
  };

  ostringstream os;
  uint32_t      nibblesCount = 0;
  uint32_t      bytesCount   = 0;

  auto appendNibble = [&](string_view nibble)
  {
    if ((nibblesCount != 0) && ((nibblesCount % 2) == 0))
    {
      ++bytesCount;
      if (bytesCount == bytesPerLine)
      {
        nibblesCount = 0;
        bytesCount   = 0;

        os << eolSeparator << std::endl;
      }
      else if (nibblesCount == 4)
      {
        os << quadSeparator;
      }
      else if (nibblesCount == 8)
      {
        nibblesCount = 0;
        os << octoSeparator;
      }
    }

    ++nibblesCount;
    os << nibble;
  };

  if (prefixWith0x)
  {
    os << "0x";
  }


  auto bitsCount = m_usedBits;

  for (auto byte : m_data)
  {
    if (bitsCount >= 8)
    {
      appendNibble(nibbles[byte >> 4]);
      appendNibble(nibbles[byte &  0x0F]);
      bitsCount -= 8;
    }
    else if (bitsCount > 4)
    {
      appendNibble(nibbles[byte >> 4]);
      bitsCount -= 4;
      appendNibble(nibbles[byte & LEFT_BITS_MASK_4[bitsCount]]);
    }
    else
    {
      byte >>= 4;         // Put bits on least significant nibble
      appendNibble(nibbles[byte & LEFT_BITS_MASK_4[bitsCount]]);
    }
  }

  return os.str();
}
//
//  End of: BinaryVector::DataAsHexString
//---------------------------------------------------------------------------


//! Gets content as formatted hexadecimal or/and binary string
//!
//!
//! @note An example of formatting is: 0xFACE_DEAD:BEEF_0123:CAFE_/b01
//!
//! @param hexStyleThreshold  The number of bits that makes the result starting as hex string (preference is to be >= 8)
//! @param quadSeparator      Characters to insert every 4 digits
//! @param octaSeparator      Characters to insert every 8 digits
//! @param bytesPerLine       Number of bytes (sequence of 8 bits) to write per line.
//!                           When zero, all is on the "same line"
//! @param eolSeparator       Characters to insert just before new lines character (when bytesPerLine != 0)
//!
string BinaryVector::DataAsMixString (uint32_t    hexStyleThreshold,
                                      string_view quadSeparator,
                                      string_view octaSeparator,
                                      uint32_t    bytesPerLine,
                                      string_view eolSeparator) const
{
  if (m_usedBits == 0)
  {
    return "";
  }

  if (m_usedBits < hexStyleThreshold)
  {
    return DataAsBinaryString(quadSeparator, octaSeparator, bytesPerLine, eolSeparator, PREFIX_WITH_BASE_ID);
  }

  auto smartString       = DataAsHexString(quadSeparator, octaSeparator, bytesPerLine, eolSeparator, PREFIX_WITH_BASE_ID);
  auto lastByteBitsCount = m_usedBits % 8;
  auto lastQuadBitsCount = m_usedBits % 4;

  if (lastQuadBitsCount != 0)
  {
    // ---------------- Replace last digit with its binary equivalent
    //
    auto shiftCount = (lastByteBitsCount < 4) ? 8u - lastQuadBitsCount    // For cases last bits are on msb
                                              : 4u - lastQuadBitsCount;   // For cases last bits are on lsb
    auto byte       = (m_data.back() >> shiftCount) & 0x0F;
    auto lastBits   = BINARY_NIBBLES[byte].substr(4u - lastQuadBitsCount, lastQuadBitsCount);
    smartString     = smartString.substr(0, smartString.length() - 1) + "/b"s + string(lastBits);
  }
  return smartString;
}
//
//  End of: BinaryVector::DataAsMixString
//---------------------------------------------------------------------------


//! Gets content formatted as ICL binary string - AS SAVED INTERNALLY - (bits are appended from left to right)
//!
//! @note An example of formatting is: 23'b0001_1111:0011_0100:0101_010
//!
string BinaryVector::DataAsICLBinaryString () const
{
  ostringstream os;
  os << m_usedBits << "'b" << DataAsBinaryString("_", "_", 0, "", !PREFIX_WITH_BASE_ID);
  return os.str();
}
//
//  End of: BinaryVector::DataAsICLBinaryString
//---------------------------------------------------------------------------



//! Gets content formatted as ICL hexadecimal string - AS SAVED INTERNALLY - (bits are appended from left to right)
//!
//! @note An example of formatting is: 22'hface_dead_beef_0123_cafe_4 (where last '4' may mean '0b0100' or '0b010' or '0b01')
//! @note FOR PRECISE DISPLAY OF LAST BITS, please use DataAsMixString (or DataAsBinaryString)
//!
string BinaryVector::DataAsICLHexString () const
{
  ostringstream os;
  os << m_usedBits << "'h" << DataAsHexString("_", "_", 0, "", !PREFIX_WITH_BASE_ID);
  return os.str();
}
//
//  End of: BinaryVector::DataAsICLHexString
//---------------------------------------------------------------------------



//! Gets content formatted as ICL as hexadecimal or/and binary string
//!
//!
//! @note An example of formatting is: 0xFACE_DEAD:BEEF_0123:CAFE_/b01
//!
//! @param hexStyleThreshold  The number of bits that makes the result starting as hex string (preference is to be >= 8)
//!
string BinaryVector::DataAsICLMixString (uint32_t hexStyleThreshold) const
{
  if (m_usedBits == 0)
  {
    return "";
  }

  hexStyleThreshold = std::max(hexStyleThreshold, 4u);  // Must ensure that reported value is not misleading (last nibble incomplete but reported in hexadecimal)
  if (m_usedBits < hexStyleThreshold)
  {
    return DataAsICLBinaryString();
  }

  // ---------------- Deal with plain nibbles
  //
  auto plainNibblesCount = m_usedBits / 4u;
  auto lastQuadBitsCount = m_usedBits % 4;
  auto lastByteBitsCount = m_usedBits % 8;

  ostringstream os;
  os << plainNibblesCount * 4u << "'h" << DataAsHexString("_", "_", 0, "", !PREFIX_WITH_BASE_ID);

  auto smartString = os.str();

  if (lastQuadBitsCount != 0)
  {
    smartString.pop_back();
  }

  if (smartString.back() == '_')
  {
    smartString.pop_back();
  }

  // ---------------- Deals with remaining bits
  //
  if (lastQuadBitsCount != 0)
  {
    // ---------------- Replace last digit with its binary equivalent
    //
    auto shiftCount = (lastByteBitsCount < 4) ? 8u - lastQuadBitsCount    // For cases last bits are on msb
                                              : 4u - lastQuadBitsCount;   // For cases last bits are on lsb
    auto byte       = (m_data.back() >> shiftCount) & 0x0F;
    auto lastBits   = BINARY_NIBBLES[byte].substr(4u - lastQuadBitsCount, lastQuadBitsCount);

    smartString.append(", ").append(to_string(lastQuadBitsCount)).append("'b").append(string(lastBits));
  }
  return smartString;
}
//
//  End of: BinaryVector::DataAsICLHexString
//---------------------------------------------------------------------------

//! Returns data right aligned in a new buffer
//!
//! @note Internal representation is left aligned.
//!       For example for a 23 bits BinaryVector with value 0x654321 (0b110_0101_0100_0011_0010_0001)
//!       Will be in memory (with increased address)
//!       Left aligned:   [CA][86][42]  (0b11001010:10000110:01000010)
//!       Right aligned:  [65][43][21]  (0b01100101:01000011:00100001)
//!
//! @note To be cache friendly data are processed in increased address order
vector<uint8_t> BinaryVector::DataRightAligned () const
{
  vector<uint8_t> rightAligned;

  auto binVectorLsbBits = LastByteBitsCount(); // This is the number of meaningful bits on last byte in BinaryVector representation (left aligned)

  if (binVectorLsbBits == 0) // Deal with fast case
  {
    rightAligned = m_data;
  }
  else     // Deal with case, right aligned bytes are stranded on two bytes of left aligned version
  {
    rightAligned.reserve(m_data.size());

    auto pBegin = m_data.cbegin();
    auto pEnd   = m_data.cend();
    auto pByte  = pBegin;

    uint8_t  bufferShiftCount   = 8u - binVectorLsbBits;  // This is the number of meaningful bits on first byte in right aligned result
    uint32_t remainingBitsCount = m_usedBits;
    uint8_t  byte = 0;

    // ---------------- Process first bits (on first byte)
    //
    byte   = *pByte;
    byte >>= bufferShiftCount;
    byte  &= RIGHT_BITS_MASK_8[binVectorLsbBits];

    rightAligned.push_back(byte);

    // ---------------- Process other bytes
    //
    remainingBitsCount -= binVectorLsbBits;
    while ((remainingBitsCount != 0) && (pByte < pEnd))
    {
      uint8_t msb = *pByte++;   // msb for result is on lsb of "left" byte of binary BinaryVector
      uint8_t lsb = *pByte;     // lsb for result is on msb of "right" byte of binary BinaryVector

      // ---------------- Merge bytes
      //
      // e.g. : [xxxmmmmm][lllyyyyy] ==> [mmmmmlll]
      //           msb        lsb           byte
      //
      msb <<= binVectorLsbBits;
      lsb >>= bufferShiftCount;
      msb &= LEFT_BITS_MASK_8[bufferShiftCount];
      lsb &= RIGHT_BITS_MASK_8[binVectorLsbBits];

      uint8_t byte = msb | lsb;
      rightAligned.push_back(byte);

      remainingBitsCount -= 8;
    }
  }

  return rightAligned;
}
//
//  End of: BinaryVector::DataRightAligned
//---------------------------------------------------------------------------



//! Returns the number of leading bits that are set to zero
//!
uint32_t BinaryVector::LeadingZeroesCount () const
{
  uint32_t count         = 0u;
  uint32_t remainingBits = m_usedBits;

  for (const auto bits : m_data)
  {
    if (bits == 0u)
    {
      if (remainingBits >= 8u)
      {
        count         += 8u;
        remainingBits -= 8u;
      }
      else
      {
        count += remainingBits;
        break;
      }
    }
    else
    {
      constexpr uint32_t lut[16] = {
                                     4u,   // 0x0
                                     3u,   // 0x1
                                     2u,   // 0x2
                                     2u,   // 0x3
                                     1u,   // 0x4
                                     1u,   // 0x5
                                     1u,   // 0x6
                                     1u,   // 0x7
                                     0u,   // 0x8
                                     0u,   // 0x9
                                     0u,   // 0xA
                                     0u,   // 0xB
                                     0u,   // 0xC
                                     0u,   // 0xD
                                     0u,   // 0xE
                                     0u,   // 0xF
                                   };

      auto lastCount = lut[bits >> 4];  // Inspect upper nibble
      if (lastCount == 4u)
      {
        lastCount += lut[bits & 0x0F];  // Inspect lower nibble
      }
      lastCount = std::min(lastCount, remainingBits);  // Ignore bits that are not part of the BinaryVector

      count += lastCount;
      break;
    }
  }

  return count;
}
//
//  End of: BinaryVector::LeadingZeroesCount
//---------------------------------------------------------------------------



//! Defines used based from leading char of number string
//!
NumberBase BinaryVector::NumberBaseForValuePrefix (string_view number)
{
  if (!number.empty())
  {
    auto baseChar = number.front();
    switch (baseChar)
    {
      case 'b':
      case 'B':
        return NumberBase::Binary;
      case 'h':
      case 'H':
      case 'x':
      case 'X':
        return NumberBase::Hexadecimal;
      case 'd':
      case 'D':
        return NumberBase::Decimal;
      case 'o':
      case 'O':
      return NumberBase::Octal;
      default:
        return NumberBase::Undefined;
    }
  }

  return NumberBase::Undefined;
}
//
//  End of: BinaryVector::NumberBaseForValuePrefix
//---------------------------------------------------------------------------


//! Copy assignment
//!
//! @note Does not change the fixed size property
//!
BinaryVector& BinaryVector::operator= (const BinaryVector& rhs)
{
  CHECK_FIXED_SIZE_ASSIGNMENT(rhs.m_usedBits);

  m_data         = rhs.m_data;
  m_usedBits     = rhs.m_usedBits;
  m_sizeProperty = rhs.m_sizeProperty == SizeProperty::FixedOnCopy ? SizeProperty::FixedOnCopy : m_sizeProperty;

  return *this;
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------


//! Move assignment
//!
//! @note Does not change the fixed size property
//!
BinaryVector& BinaryVector::operator= (BinaryVector&& rhs)
{
  if (this != &rhs)
  {
    CHECK_FIXED_SIZE_ASSIGNMENT(rhs.m_usedBits);
    m_data         = std::move(rhs.m_data);
    m_usedBits     = rhs.m_usedBits;
    m_sizeProperty = rhs.m_sizeProperty == SizeProperty::FixedOnCopy ? SizeProperty::FixedOnCopy : m_sizeProperty;

    rhs.m_usedBits     = 0;
    rhs.m_sizeProperty = SizeProperty::NotFixed;
  }

  return *this;
}
//
//  End of: BinaryVector::BinaryVector
//---------------------------------------------------------------------------






//! Returns another BinaryVector with every bits toggles
//!
BinaryVector BinaryVector::operator~ () const
{
  BinaryVector toggled(*this);

  toggled.ToggleBits();
  return toggled;
}
//
//  End of: BinaryVector::operator~
//---------------------------------------------------------------------------


//! Bitwise and with another vector
//!
BinaryVector& BinaryVector::operator&= (const BinaryVector& rhs)
{
  CHECK_SAME_SIZE(rhs);

  auto left  = m_data.begin();
  auto right = rhs.m_data.cbegin();

  while (left != m_data.end())
  {
    *left++ &= *right++;
  }

  MaskLastByte();

  return *this;
}
//
//  End of: BinaryVector::operator&=
//---------------------------------------------------------------------------


//! Bitwise or with another vector
//!
BinaryVector& BinaryVector::operator|= (const BinaryVector& rhs)
{
  CHECK_SAME_SIZE(rhs);

  auto left  = m_data.begin();
  auto right = rhs.m_data.cbegin();

  while (left != m_data.end())
  {
    *left++ |= *right++;
  }

  MaskLastByte();

  return *this;
}
//
//  End of: BinaryVector::operator&=
//---------------------------------------------------------------------------


//! Bitwise xor with another vector
//!
BinaryVector& BinaryVector::operator^= (const BinaryVector& rhs)
{
  CHECK_SAME_SIZE(rhs);

  auto left  = m_data.begin();
  auto right = rhs.m_data.cbegin();

  while (left != m_data.end())
  {
    *left++ ^= *right++;
  }

  MaskLastByte();

  return *this;
}
//
//  End of: BinaryVector::operator&=
//---------------------------------------------------------------------------


//! Concatenate two scan vectors
//!
BinaryVector BinaryVector::operator+ (const BinaryVector& rhs) const
{
  BinaryVector result(*this);

  result.Append(rhs);

  return result;
}
//
//  End of: BinaryVector::operator~
//---------------------------------------------------------------------------

//! Return data from BinaryVector as uint8 vector : NB no checks are done
//!
//! @param value  Variable to update with current value
//!
std::vector<uint8_t> BinaryVector::Get_DataVector () const
{
  return m_data;
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------

//! Reads 8 bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 8 bits, then a padding will be appled on value most significants bits
//! @note If the BinaryVector has more than 8 bits, then the value will take only the least significants bits
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (uint8_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint32_t lastByteOffset = m_data.size() - 1u;

  value = (m_usedBits > 0) ? MergeToByte(lastByteOffset, lastByteBits) : 0;
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------




//! Reads 16 bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 16 bits, then a padding will be appled on value most significants bits
//! @note If the BinaryVector has more than 16 bits, then the value will take only the least significants bits
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (uint16_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint32_t lastByteOffset = m_data.size() - 1u;

  uint8_t value_0 =  (m_usedBits > 0)  ? MergeToByte(lastByteOffset,      lastByteBits) : 0;  // LSB
  uint8_t value_1 =  (m_usedBits > 8)  ? MergeToByte(lastByteOffset - 1u, lastByteBits) : 0;  // MSB

  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    asBytes[0] = value_0;
    asBytes[1] = value_1;
  #else
    asBytes[0] = value_1;
    asBytes[1] = value_0;
  #endif
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------


//! Reads 32 bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 32 bits, then a padding will be appled on value most significants bits
//! @note If the BinaryVector has more than 32 bits, then the value will take only the least significants bits
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (uint32_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint32_t lastByteOffset = m_data.size() - 1u;

  uint8_t value_0 =  (m_usedBits > 0)  ? MergeToByte(lastByteOffset,      lastByteBits) : 0;  // LSB
  uint8_t value_1 =  (m_usedBits > 8)  ? MergeToByte(lastByteOffset - 1u, lastByteBits) : 0;
  uint8_t value_2 =  (m_usedBits > 16) ? MergeToByte(lastByteOffset - 2u, lastByteBits) : 0;
  uint8_t value_3 =  (m_usedBits > 24) ? MergeToByte(lastByteOffset - 3u, lastByteBits) : 0;  // MSB

  // ---------------- Combine all bytes dealing with endianness
  //
  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    asBytes[0] = value_0;
    asBytes[1] = value_1;
    asBytes[2] = value_2;
    asBytes[3] = value_3;
  #else
    asBytes[0] = value_3;
    asBytes[1] = value_2;
    asBytes[2] = value_1;
    asBytes[3] = value_0;
  #endif
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------


//! Reads 64 bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 64 bits, then a padding will be appled on value most significants bits
//! @note If the BinaryVector has more than 64 bits, then the value will take only the least significants bits
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (uint64_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint32_t lastByteOffset = m_data.size() - 1u;

  uint8_t value_0 =  (m_usedBits > 0)  ? MergeToByte(lastByteOffset,      lastByteBits) : 0;  // LSB
  uint8_t value_1 =  (m_usedBits > 8)  ? MergeToByte(lastByteOffset - 1u, lastByteBits) : 0;
  uint8_t value_2 =  (m_usedBits > 16) ? MergeToByte(lastByteOffset - 2u, lastByteBits) : 0;
  uint8_t value_3 =  (m_usedBits > 24) ? MergeToByte(lastByteOffset - 3u, lastByteBits) : 0;
  uint8_t value_4 =  (m_usedBits > 32) ? MergeToByte(lastByteOffset - 4u, lastByteBits) : 0;
  uint8_t value_5 =  (m_usedBits > 40) ? MergeToByte(lastByteOffset - 5u, lastByteBits) : 0;
  uint8_t value_6 =  (m_usedBits > 48) ? MergeToByte(lastByteOffset - 6u, lastByteBits) : 0;
  uint8_t value_7 =  (m_usedBits > 56) ? MergeToByte(lastByteOffset - 7u, lastByteBits) : 0; // MSB

  // ---------------- Combine all bytes dealing with endianness
  //
  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    asBytes[0] = value_0;
    asBytes[1] = value_1;
    asBytes[2] = value_2;
    asBytes[3] = value_3;
    asBytes[4] = value_4;
    asBytes[5] = value_5;
    asBytes[6] = value_6;
    asBytes[7] = value_7;
  #else
    asBytes[0] = value_7;
    asBytes[1] = value_6;
    asBytes[2] = value_5;
    asBytes[3] = value_4;
    asBytes[4] = value_3;
    asBytes[5] = value_2;
    asBytes[6] = value_1;
    asBytes[7] = value_0;
  #endif
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------


//! Reads 8 signed bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 8 bits, then a padding will be appled on value most significants bits (with sign extension)
//! @note If the BinaryVector has more than 8 bits, then the value will take only the least significants bits (truncation occurs)
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (int8_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint32_t lastByteOffset = m_data.size() - 1u;

  value = MergeToByte(lastByteOffset, lastByteBits, true);
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------


//! Reads 16 signed bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 8 bits, then a padding will be appled on value most significants bits (with sign extension)
//! @note If the BinaryVector has more than 8 bits, then the value will take only the least significants bits (truncation occurs)
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (int16_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint32_t lastByteOffset = m_data.size() - 1u;
  uint8_t  fillBits       = IsNegative() ? 0xFF : 0x00;

  uint8_t value_0 =  (m_usedBits > 0)  ? MergeToByte(lastByteOffset,      lastByteBits, true) : fillBits;  // LSB
  uint8_t value_1 =  (m_usedBits > 8)  ? MergeToByte(lastByteOffset - 1u, lastByteBits, true) : fillBits;  // MSB

  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    asBytes[0] = value_0;
    asBytes[1] = value_1;
  #else
    asBytes[0] = value_1;
    asBytes[1] = value_0;
  #endif
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------


//! Reads 32 signed bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 8 bits, then a padding will be appled on value most significants bits (with sign extension)
//! @note If the BinaryVector has more than 8 bits, then the value will take only the least significants bits (truncation occurs)
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (int32_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint32_t lastByteOffset = m_data.size() - 1u;
  uint8_t  fillBits       = IsNegative() ? 0xFF : 0x00;

  uint8_t value_0 = (m_usedBits > 0)  ? MergeToByte(lastByteOffset,       lastByteBits, true) : fillBits; // LSB
  uint8_t value_1 = (m_usedBits > 8)  ? MergeToByte(lastByteOffset  - 1u, lastByteBits, true) : fillBits;
  uint8_t value_2 = (m_usedBits > 16) ? MergeToByte(lastByteOffset  - 2u, lastByteBits, true) : fillBits;
  uint8_t value_3 = (m_usedBits > 24) ? MergeToByte(lastByteOffset  - 3u, lastByteBits, true) : fillBits; // MSB

  // ---------------- Combine all bytes dealing with endianness
  //
  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    asBytes[0] = value_0;
    asBytes[1] = value_1;
    asBytes[2] = value_2;
    asBytes[3] = value_3;
  #else
    asBytes[0] = value_3;
    asBytes[1] = value_2;
    asBytes[2] = value_1;
    asBytes[3] = value_0;
  #endif
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------


//! Reads 64 signed bits value from BinaryVector
//!
//! @note This is an invalid operation when the BinaryVector is empty
//! @note If the BinaryVector has less than 8 bits, then a padding will be appled on value most significants bits (with sign extension)
//! @note If the BinaryVector has more than 8 bits, then the value will take only the least significants bits (truncation occurs)
//!
//! @param value  Variable to update with current value
//!
void BinaryVector::Get (int64_t&  value) const
{
  CHECK_NOT_EMPTY;

  uint8_t  lastByteBits   = LastByteBitsCount();
  uint64_t lastByteOffset = m_data.size() - 1u;
  uint8_t  fillBits       = IsNegative() ? 0xFF : 0x00;

  uint8_t value_0 = (m_usedBits > 0)  ? MergeToByte(lastByteOffset,      lastByteBits, true) : fillBits; // LSB
  uint8_t value_1 = (m_usedBits > 8)  ? MergeToByte(lastByteOffset - 1u, lastByteBits, true) : fillBits;
  uint8_t value_2 = (m_usedBits > 16) ? MergeToByte(lastByteOffset - 2u, lastByteBits, true) : fillBits;
  uint8_t value_3 = (m_usedBits > 24) ? MergeToByte(lastByteOffset - 3u, lastByteBits, true) : fillBits; // MSB
  uint8_t value_4 = (m_usedBits > 32) ? MergeToByte(lastByteOffset - 4u, lastByteBits, true) : fillBits; // LSB
  uint8_t value_5 = (m_usedBits > 40) ? MergeToByte(lastByteOffset - 5u, lastByteBits, true) : fillBits;
  uint8_t value_6 = (m_usedBits > 48) ? MergeToByte(lastByteOffset - 6u, lastByteBits, true) : fillBits;
  uint8_t value_7 = (m_usedBits > 56) ? MergeToByte(lastByteOffset - 7u, lastByteBits, true) : fillBits; // MSB

  // ---------------- Combine all bytes dealing with endianness
  //
  auto asBytes = reinterpret_cast<uint8_t*>(&value);
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    asBytes[0] = value_0;
    asBytes[1] = value_1;
    asBytes[2] = value_2;
    asBytes[3] = value_3;
    asBytes[4] = value_4;
    asBytes[5] = value_5;
    asBytes[6] = value_6;
    asBytes[7] = value_7;
  #else
    asBytes[0] = value_7;
    asBytes[1] = value_6;
    asBytes[2] = value_5;
    asBytes[3] = value_4;
    asBytes[4] = value_3;
    asBytes[5] = value_2;
    asBytes[6] = value_1;
    asBytes[7] = value_0;
  #endif
}
//
//  End of: BinaryVector::Get
//---------------------------------------------------------------------------

//! Masks last byte to be sure that unused bits are always set to zero
//!
void BinaryVector::MaskLastByte ()
{
  auto bitsOnLastByte = m_usedBits % 8;
  if (bitsOnLastByte != 0)
  {
    m_data.back() &= LEFT_BITS_MASK_8[bitsOnLastByte];
  }
}
//
//  End of: BinaryVector::MaskLastByte
//---------------------------------------------------------------------------


//! Merges bits from two bytes viewed as signed
//!
//! @param lsbOffset      Rightmost byte to merge
//! @param lsbBitsCount   Number of bits to take from rightmost byte
//! @param asSigned       When true underlying value is considered signed
//!
uint8_t BinaryVector::MergeToByte (uint32_t lsbOffset, uint8_t lsbBitsCount, bool asSigned) const
{
  const uint8_t secondToLastByteBits = 8u - lsbBitsCount;

  uint8_t value_L = m_data[lsbOffset] >> secondToLastByteBits;

  if (lsbOffset == 0)
  {
    if (asSigned && IsNegative())
    {
      value_L |= LEFT_BITS_MASK_8[secondToLastByteBits];
    }
    return value_L;
  }

  auto value_H = m_data[lsbOffset - 1u] << lsbBitsCount;
  auto value   = value_H | value_L;

  return value;
}
//
//  End of: BinaryVector::MergeToByte
//---------------------------------------------------------------------------


//! Insert bits with patern times before current bits
//!
//! @param bitsCount      Number of bits of resulting BinaryVector
//! @param fillPattern    Filling patern (may be truncated at the end)
//!
BinaryVector& BinaryVector::PrependBits (uint32_t bitsCount, uint8_t fillPattern)
{
  BinaryVector tmp(bitsCount, fillPattern);

  tmp.Append(*this);

  *this = std::move(tmp);
  return *this;
}
//
//  End of: BinaryVector::PrependBits
//---------------------------------------------------------------------------


//! Sets from N signed bits
//!
template<typename T>
void BinaryVector::SetSigned (T value)
{
  static_assert(std::is_signed<T>::value, "Must only be called for signed value as it includes sign bit extension");
  auto constexpr valueBitsCount   = uint32_t(8u * sizeof(value));
  auto           initialBitsCount = m_usedBits;

  Clear();

  if (!FixedSize() || (initialBitsCount == 0))
  {
    Append(static_cast<std::make_unsigned_t<T>>(value));
  }
  else
  {
    m_sizeProperty = SizeProperty::NotFixed;
    AT_SCOPE_EXIT([this]() { m_sizeProperty = SizeProperty::Fixed; });

    auto bitsCountToAppend = initialBitsCount;

    if (initialBitsCount > valueBitsCount)
    {
      auto msbCount = initialBitsCount - valueBitsCount;

      AppendBits(value < 0, msbCount);
      bitsCountToAppend = valueBitsCount;
    }

    Append(static_cast<std::make_unsigned_t<T>>(value), bitsCountToAppend);
  }
}


//! Sets from N unsigned bits
//!
template<typename T>
void BinaryVector::SetUnsigned (T value)
{
  static_assert(std::is_unsigned<T>::value, "Must only be called for unsigned value as it DOES NOT includes sign bit extension");

  auto constexpr valueBitsCount   = uint32_t(8u * sizeof(value));
  auto           initialBitsCount = m_usedBits;

  Clear();

  if (!FixedSize() || (initialBitsCount == 0))
  {
    Append(value);
  }
  else
  {
    m_sizeProperty = SizeProperty::NotFixed;
    AT_SCOPE_EXIT([this]() { m_sizeProperty = SizeProperty::Fixed; });

    auto bitsCountToAppend = initialBitsCount;

    if (initialBitsCount > valueBitsCount)
    {
      auto msbCount = initialBitsCount - valueBitsCount;

      AppendBits(false, msbCount);
      bitsCountToAppend = valueBitsCount;
    }

    Append(value, bitsCountToAppend);
    m_sizeProperty = SizeProperty::Fixed;
  }
}


//! Sets from 8 bits
//!
void BinaryVector::Set (int8_t value)
{
  SetSigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------


//! Sets from 16 bits
//!
void BinaryVector::Set (int16_t value)
{
  SetSigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------


//! Sets from 32 bits
//!
void BinaryVector::Set (int32_t value)
{
  SetSigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------


//! Sets from 64 bits
//!
void BinaryVector::Set (int64_t value)
{
  SetSigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------


//! Sets from 8 bits
//!
void BinaryVector::Set (uint8_t value)
{
  SetUnsigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------


//! Sets from 16 bits
//!
void BinaryVector::Set (uint16_t value)
{
  SetUnsigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------

//! Sets from 32 bits
//!
void BinaryVector::Set (uint32_t value)
{
  SetUnsigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------


//! Sets from 64 bits
//!
void BinaryVector::Set (uint64_t value)
{
  SetUnsigned(value);
}
//
//  End of: BinaryVector::Set
//---------------------------------------------------------------------------




//! Sets specified bit (to one)
//!
//! @param bitOffset Zero based bit offset (from left)
//!
void BinaryVector::SetBit (uint32_t bitOffset)
{
  CHECK_PARAMETER_LT(bitOffset, m_usedBits, "Out of range bit id: "s + to_string(bitOffset));

  auto byteOffset      = bitOffset / 8;
  auto bitOffsetInByte = bitOffset % 8;

  m_data[byteOffset] |= BIT_MASK_8[bitOffsetInByte];
}
//
//  End of: BinaryVector::SetBit
//---------------------------------------------------------------------------


//! Assigns a portion of the BinaryVector
//!
//! @note It is optimized for small number of copied bytes
//!
//! @param startOffset  Zero based bit offset (from left)
//! @param value        Other BinaryVector to assign to slice
//!
//! @return Reference to this (to allow cascading calls)
BinaryVector& BinaryVector::SetSlice (uint32_t startOffset, const BinaryVector& value)
{
  return SetSlice_Impl(startOffset, value.BitsCount(), value.DataLeftAligned());
}


//! Assigns an unsigned 8 bits into a slice
//!
//! @note Range left/right boundaries are reversed relative to bits numbering in
//!       BinaryVector.
//!       BinaryVector has it leftmost bits identified at offset 0 while an ICL
//!       vector range is usually numbered from its rightmost bit
//!
//!         e.g. an IndexedRange(7, 0):
//!           - relative to a  8 bits BinaryVector is equivalent to bits offsets [0, 7]
//!           - relative to a 10 bits BinaryVector is equivalent to bits offsets [2, 9]
//!
//!         e.g. an IndexedRange(0, 7):
//!           ? relative to a  8 bits BinaryVector is equivalent to bits offsets [7, 0]
//!           ? relative to a 10 bits BinaryVector is equivalent to bits offsets [9, 2]
//!           ? in that cases, values are reversed before being effectively assigned
//!
//! @param range  Ranges of bits to assign
//! @param value  Value to assign
//!
//! @return Reference to this (to allow cascading calls)
BinaryVector& BinaryVector::SetSlice (IndexedRange range, uint8_t value)
{
  return SetSlice_T(range, value);
}
//
//  End of: BinaryVector::SetSlice
//---------------------------------------------------------------------------


//! Assigns an unsigned 16 bits into a slice
//!
//! @note Range left/right boundaries are reversed relative to bits numbering in
//!       BinaryVector.
//!       BinaryVector has it leftmost bits identified at offset 0 while an ICL
//!       vector range is usually numbered from its rightmost bit
//!
//!         e.g. an IndexedRange(7, 0):
//!           - relative to a  8 bits BinaryVector is equivalent to bits offsets [0, 7]
//!           - relative to a 10 bits BinaryVector is equivalent to bits offsets [2, 9]
//!
//!         e.g. an IndexedRange(0, 7):
//!           ? relative to a  8 bits BinaryVector is equivalent to bits offsets [7, 0]
//!           ? relative to a 10 bits BinaryVector is equivalent to bits offsets [9, 2]
//!           ? in that cases, values are reversed before being effectively assigned
//!
//! @param range  Ranges of bits to assign
//! @param value  Value to assign
//!
//! @return Reference to this (to allow cascading calls)
BinaryVector& BinaryVector::SetSlice (IndexedRange range, uint16_t value)
{
  return SetSlice_T(range, value);
}


//! Assigns an unsigned 32 bits into a slice
//!
//! @note Range left/right boundaries are reversed relative to bits numbering in
//!       BinaryVector.
//!       BinaryVector has it leftmost bits identified at offset 0 while an ICL
//!       vector range is usually numbered from its rightmost bit
//!
//!         e.g. an IndexedRange(7, 0):
//!           - relative to a  8 bits BinaryVector is equivalent to bits offsets [0, 7]
//!           - relative to a 10 bits BinaryVector is equivalent to bits offsets [2, 9]
//!
//!         e.g. an IndexedRange(0, 7):
//!           ? relative to a  8 bits BinaryVector is equivalent to bits offsets [7, 0]
//!           ? relative to a 10 bits BinaryVector is equivalent to bits offsets [9, 2]
//!           ? in that cases, values are reversed before being effectively assigned
//!
//! @param range  Ranges of bits to assign
//! @param value  Value to assign
//!
//! @return Reference to this (to allow cascading calls)
BinaryVector& BinaryVector::SetSlice (IndexedRange range, uint32_t value)
{
  return SetSlice_T(range, value);
}

//! Assigns an unsigned 64 bits into a slice
//!
//! @note Range left/right boundaries are reversed relative to bits numbering in
//!       BinaryVector.
//!       BinaryVector has it leftmost bits identified at offset 0 while an ICL
//!       vector range is usually numbered from its rightmost bit
//!
//!         e.g. an IndexedRange(7, 0):
//!           - relative to a  8 bits BinaryVector is equivalent to bits offsets [0, 7]
//!           - relative to a 10 bits BinaryVector is equivalent to bits offsets [2, 9]
//!
//!         e.g. an IndexedRange(0, 7):
//!           ? relative to a  8 bits BinaryVector is equivalent to bits offsets [7, 0]
//!           ? relative to a 10 bits BinaryVector is equivalent to bits offsets [9, 2]
//!           ? in that cases, values are reversed before being effectively assigned
//!
//! @param range  Ranges of bits to assign
//! @param value  Value to assign
//!
//! @return Reference to this (to allow cascading calls)
BinaryVector& BinaryVector::SetSlice (IndexedRange range, uint64_t value)
{
  return SetSlice_T(range, value);
}



//! Assigns an unsigned N bits into a slice
//!
//! @note Range left/right boundaries are reversed relative to bits numbering in
//!       BinaryVector.
//!       BinaryVector has it leftmost bits identified at offset 0 while an ICL
//!       vector range is usually numbered from its rightmost bit
//!
//!         e.g. an IndexedRange(7, 0):
//!           - relative to a  8 bits BinaryVector is equivalent to bits offsets [0, 7]
//!           - relative to a 10 bits BinaryVector is equivalent to bits offsets [2, 9]
//!
//!         e.g. an IndexedRange(0, 7):
//!           ? relative to a  8 bits BinaryVector is equivalent to bits offsets [7, 0]
//!           ? relative to a 10 bits BinaryVector is equivalent to bits offsets [9, 2]
//!           ? in that cases, values are reversed before being effectively assigned
//!
//! @param range  Ranges of bits to assign
//! @param value  Value to assign
//!
//! @return Reference to this (to allow cascading calls)
template<typename T>
BinaryVector& BinaryVector::SetSlice_T (IndexedRange range, T value)
{
  CHECK_FALSE(IsEmpty(),                           "Cannot set slice of an empty BinaryVector");
  CHECK_PARAMETER_LTE (range.Width(), BitsCount(), "Range is too large");
  CHECK_PARAMETER_LT  (range.right,   BitsCount(), "Range extends past destination boundary");

  CHECK_PARAMETER_TRUE(   range.IsSingleBit()
                       || range.IncreasesTowardRight(), "Do not support reversed range yet");

  auto rangeBitsCount = range.Width();

  auto constexpr valueBitsCount = 8u * sizeof(T);

  auto appendBitsCount = rangeBitsCount;

  BinaryVector asBinaryVector;

  // ---------------- Assigmnent into larger chunk
  //
  if (rangeBitsCount > valueBitsCount) // Must assign into a larger chunk ?
  {
    // ---------------- Place value into a BinaryVector of proper bits count first
    //
    asBinaryVector.AppendBits(false, rangeBitsCount - valueBitsCount);
    appendBitsCount = valueBitsCount;
  }
  else if (rangeBitsCount < valueBitsCount)
  {
    auto maxValue = MAX_UNSIGNED_VALUE_FOR_BITS_COUNT[rangeBitsCount];
    CHECK_PARAMETER_LTE(value, maxValue, "Value is too large for target range");
  }

  asBinaryVector.Append(value, appendBitsCount, BitsAlignment::Right);

  return SetSlice_Impl(range.left, rangeBitsCount, asBinaryVector.DataLeftAligned());
}
//
//  End of: BinaryVector::SetSlice_T
//---------------------------------------------------------------------------


//! Assigns a portion of the BinaryVector
//!
//! @note It is optimized for small number of copied bytes
//!
//! @param startOffset  Zero based bit offset (from left)
//! @param bitsCount    Number of bits to assign
//! @param source       Points to, left aligned, source data
//!
//! @return Reference to this (to allow cascading calls)
//!
BinaryVector& BinaryVector::SetSlice_Impl (uint32_t startOffset, uint32_t bitsCount, const uint8_t* source)
{
  CHECK_PARAMETER_NOT_NULL(source,                          "Source must not be nullptr");
  CHECK_FALSE(IsEmpty(),                                    "Cannot set slice of an empty BinaryVector");
  CHECK_PARAMETER_RANGE(startOffset, 0u, BitsCount() - 1u,  "Out of range offset: "s + std::to_string(startOffset));
  CHECK_PARAMETER_LTE(startOffset + bitsCount, BitsCount(), "Slice is partially out of range");

  const auto srcBitsCount       = bitsCount;
  const auto dstByteOffset      = startOffset / 8u;
  const auto offsetInFirstBytes = startOffset % 8u;

  auto dest = m_data.data() + dstByteOffset;
  auto srce = source;

  if (offsetInFirstBytes == 0) // Are bits left aligned?
  {
    const auto srcLastByteBitsCount = srcBitsCount % 8u;
    const auto onlyFull             = srcLastByteBitsCount == 0;
    const auto fullBytesCount       = srcBitsCount / 8u;

    for (uint32_t ii = 0 ; ii < fullBytesCount ; ++ii)
    {
      *dest++ = *srce++;
    }

    if (!onlyFull)
    {
      const auto dstMask = LEFT_BITS_CLEAR_MASK_8[srcLastByteBitsCount];
      const auto srcMask = LEFT_BITS_MASK_8[srcLastByteBitsCount];

      auto dstValue = *dest & dstMask;
      auto srcValue = *srce & srcMask;

      dstValue |= srcValue;
      *dest = dstValue;
    }
  }
  else  // ==> Source bytes are split into destination bytes
  {
    auto boundaryOffset =  startOffset  % 8u;                 // Split point in destination bytes
    bool partialByte    = (boundaryOffset + srcBitsCount) < 8u; // Detect case when source spans only parts of a destination byte
    if (partialByte)
    {
      // ---------------- Example
      //                    ____
      // srce:         |abcxxxxx|               (3 bits)
      // dest:    |01274567|01234567|012yyyyy|  (19 bits)
      // result:  |012abc67|01234567|012yyyyy|
      //
      auto dstLastBitOffset = boundaryOffset + srcBitsCount - 1u;
      auto dstMask          = LEFT_BITS_MASK_8[boundaryOffset] | LEFT_BITS_CLEAR_MASK_8[dstLastBitOffset + 1]; // e.g. 0b11100000 | 0b00000011 ==> 0b11100011
      auto srcMask          = LEFT_BITS_MASK_8[srcBitsCount];                                               // e.g. 0b11100000

      auto dstValue = *dest & dstMask; // Clear bits that will be assigned
      auto srcValue = *srce & srcMask; // Keep only bits to assign

      srcValue >>= boundaryOffset;     // Align source bits with destination bits
      dstValue |=  srcValue;           // Merge source bits into destination value

      *dest = dstValue;
    }
    else
    {
      // ---------------- Examples
      //
      // With boundaryOffset = 5
      //                    ____ ___
      // srce:         |abcdefgh|ijklmxxx|          (13 bits)
      // dest:    |01274567|01234567|012yyyyy|      (19 bits)
      // result:  |01274abc|defghijk|lm2yyyyy|
      //
      // With boundaryOffset = 1
      //                    ____ ___
      // srce:     |abcdefgh|ijxxxxxx|          (10 bits)
      // dest:    |01274567|01234567|012yyyyy|  (19 bits)
      // result:  |0abcdefg|hij34567|012yyyyy|
      //
      const auto srcMsbBitsCount      = 8u - offsetInFirstBytes;
      const auto srcLastByteBitsCount = (srcBitsCount - srcMsbBitsCount) % 8u;

      const auto dstKeepMsbMask = LEFT_BITS_MASK_8[boundaryOffset];  // e.g. 0b1111_1000
      const auto srcKeepMsbMask = LEFT_BITS_MASK_8[srcMsbBitsCount]; // e.g. 0b1110_0000
      const auto srcKeepLsbMask = RIGHT_BITS_MASK_8[boundaryOffset]; // e.g. 0b0001_1111

      // Assign first dest byte
      //
      auto remainingBits = srcBitsCount;

      auto dstValue = *dest & dstKeepMsbMask; // Clear bits that will be assigned
      auto srcValue = *srce & srcKeepMsbMask; // Keep only bits to assign

      srcValue >>= boundaryOffset;        // Align source bits with destination bits
      dstValue |=  srcValue;              // Merge source bits into destination value

      *dest++ = dstValue;
      remainingBits -= srcMsbBitsCount;

      // Assign remaining bits, with potential "middle" bytes (assign full dest byte from source chunks)
      //
      while (remainingBits != 0)
      {
        auto srcValue_msb = *srce & srcKeepLsbMask;       // Keep only bits to assign
        ++srce;
        auto srcValue_lsb = *srce & srcKeepMsbMask;       // Keep only bits to assign

        srcValue_msb <<= srcMsbBitsCount;
        srcValue_lsb >>= boundaryOffset;
        srcValue = srcValue_msb | srcValue_lsb;

        if (remainingBits >= 8u) // Can assign full byte?
        {
          *dest++ = srcValue;
          remainingBits -= 8u;
        }
        else
        {
          const auto dstKeepLsbMask = LEFT_BITS_CLEAR_MASK_8[srcLastByteBitsCount]; // e.g. 0b0011_1111

          srcValue &= ~dstKeepLsbMask;

          dstValue  = *dest & dstKeepLsbMask;
          dstValue |=  srcValue;

          *dest = dstValue;
          remainingBits = 0u;
        }
      }
    }
  }

  return *this;
}
//
//  End of: BinaryVector::SetSlice
//---------------------------------------------------------------------------






//! Shifts bits left in array
//!
//! @note Example for a shift count of 3 (where bits are represented by digits, x by not used and 0 bits forced to zero)
//!   Source buffer:      [12345678_12345678_1234xxx]
//!   Destination buffer: [45678123_45678123_4xxx000]
//!
//! @param pSource        Source pointer
//! @param pDest          Destination pointer (can be same as source or at lower address)
//! @param bytesCount     Number of bytes to process
//! @param leftShiftCount Bits shift count in range [0, 7]
//!
void BinaryVector::ShiftBufferLeft (const uint8_t* pSource, uint8_t* pDest, uint32_t bytesCount, uint8_t leftShiftCount)
{
  CHECK_PARAMETER_LTE(leftShiftCount, 7, "Shift count (" + to_string(leftShiftCount)+ ") is expected to be in range [0, 7]");
  CHECK_PARAMETER_NOT_NULL(pSource, "Invalid source buffer");
  CHECK_PARAMETER_NOT_NULL(pDest,   "Invalid destination buffer");

  if (bytesCount == 0)
  {
    return;
  }

  if (leftShiftCount == 0)
  {
    std::memcpy(pDest, pSource, bytesCount);
  }
  else
  {
    auto     pByte           = pSource;
    uint32_t bitsCount       = (bytesCount * 8u) - leftShiftCount; // This is to ease detection of end of loop
    uint8_t  rightShiftCount = 8u - leftShiftCount;                // This is to align msb of next source byte to be lsb for destination byte

    while (bitsCount != 0)
    {
      uint8_t msb = *pByte++ << leftShiftCount;

      if (bitsCount <= rightShiftCount)
      {
        *pDest    = msb;
        bitsCount = 0;
      }
      else
      {
        uint8_t lsb = *pByte >> rightShiftCount;

        // ---------------- Merge bytes
        //
        // e.g. : [xxxmmmmm][lllyyyyy] ==> [mmmmmlll]
        //           msb        lsb           byte
        //
        uint8_t byte = msb | lsb;
        *pDest++ = byte;
        bitsCount -= 8u;
      }
    }
  }
}
//
//  End of: BinaryVector::ShiftBufferLeft
//---------------------------------------------------------------------------


//! Copies one buffer to another, shifting data to transform a right aligned arrangement of data to a left aligned one
//!
//! @param sourceBuffer Source buffer (with right aligned data)
//! @param sourceBuffer Destination buffer (with left aligned data)
//! @param bitsCount    Number of meaningful bits in source buffer
//!
void BinaryVector::ShiftBufferLeft (const vector<uint8_t>& sourceBuffer, vector<uint8_t>& destBuffer, uint32_t bitsCount)
{
  if (bitsCount == 0)
  {
    return;
  }

  auto bytesCount = BytesCountFromBitsCount(bitsCount);   // This is the minimal buffer size

  CHECK_PARAMETER_GTE(sourceBuffer.size(), bytesCount, "Source buffer must be large enough to hold " + to_string(bitsCount) + " bit(s)");

  if (destBuffer.size() < bytesCount)
  {
    destBuffer.resize(bytesCount) ;
  }

  auto firstByteOffset = sourceBuffer.size() - bytesCount;
  auto pSource         = sourceBuffer.data() + firstByteOffset;
  auto pDest           = destBuffer.data();
  auto leftShiftCount  = 8u - LastByteBitsCount(bitsCount);

  ShiftBufferLeft(pSource, pDest, bytesCount, leftShiftCount);

  destBuffer.resize(bytesCount);    // Can drop, now useless, end of buffer
  destBuffer.shrink_to_fit();
}
//
//  End of: BinaryVector::CreateFromRightAlignedBuffer
//---------------------------------------------------------------------------


//! Returns a slice from BinaryVector
//!
//! @note   This call is not valid if it defines a slice that exceed the actual
//!         bits count
//!
//! @param firstBitOffset Zero based offset of first bit of slice
//! @param bitsCount      Number of bits
//!
//! @return A new BinaryVector containing a copy of defined slice
BinaryVector BinaryVector::Slice (uint32_t firstBitOffset, uint32_t bitsCount) const
{
  BinaryVector result;

  if (bitsCount != 0)
  {
    // ---------------- Check parameters validity
    //
    CHECK_PARAMETER_LT  (firstBitOffset,               m_usedBits, "Slice first bit must be within bits range");
    CHECK_PARAMETER_LTE ((firstBitOffset + bitsCount), m_usedBits, "Bits count must be such that slice is within bits range");

    auto byteOffset           = firstBitOffset / 8;
    auto bitOffsetInFirstByte = firstBitOffset % 8;
    auto bitsInFirstByte      = std::min(bitsCount, 8 - bitOffsetInFirstByte);

    // ---------------- Copies first bits of source
    //
    if (bitsInFirstByte != 0)
    {
      auto byte = m_data[byteOffset];

      byte <<= bitOffsetInFirstByte;

      result.Append(byte, bitsInFirstByte, BitsAlignment::Left);

      bitsCount -= bitsInFirstByte;
      ++byteOffset;
    }

    // ---------------- Copy full bytes from source
    //
    while (bitsCount >= 8)
    {
      bitsCount -= 8;
      result.Append(m_data[byteOffset++]);
    }

    // ---------------- Copy remaining bits from source
    //
    if (bitsCount != 0)
    {
      auto byte = m_data[byteOffset];
      result.Append(byte, bitsCount, BitsAlignment::Left);
    }
  }

  return result;
}
//
//  End of: BinaryVector::Slice
//---------------------------------------------------------------------------



//! Returns a slice from BinaryVector with reversed bit order
//!
//! @note   This call is not valid if it defines a slice that exceed the actual
//!         bits count
//!
//! @param firstBitOffset Zero based offset of first bit of slice
//! @param bitsCount      Number of bits
//!
//! @return A new BinaryVector containing a copy of defined slice
BinaryVector BinaryVector::ReverseSlice (uint32_t firstBitOffset, uint32_t bitsCount) const
{
{
  BinaryVector result;

  if (bitsCount != 0)
  {
 // auto originalSliceBuffer= original.Slice(firstBitOffset,bitsCount).DataLeftAligned();
  auto originalSlice= this->Slice(firstBitOffset,bitsCount);
  auto originalSliceBuffer= originalSlice.DataLeftAligned();
  
   std::vector< uint8_t > ReverseSliceBuffer ;
   auto reverse = [](uint8_t n)
   {
    static unsigned char lookup[16] = {
        0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
        0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };
        //Index 1==0b0001 => 0b1000
        //Index 7==0b0111 => 0b1110
        //etc

        // Detailed breakdown of the math
        //  + lookup reverse of bottom nibble
        //  |       + grab bottom nibble
        //  |       |        + move bottom result into top nibble
        //  |       |        |     + combine the bottom and top results 
        //  |       |        |     | + lookup reverse of top nibble
        //  |       |        |     | |       + grab top nibble
        //  V       V        V     V V       V
        // (lookup[n&0b1111] << 4) | lookup[n>>4]

      // Reverse the top and bottom nibble then swap them.
   return (lookup[n&0b1111] << 4) | lookup[n>>4];
   };
  
   
   // Swap bytes while reversing them
  
   int32_t index_hi= originalSlice.BytesCount()-1;
   while (index_hi>=0)
    {
    ReverseSliceBuffer.push_back(reverse(originalSliceBuffer[index_hi]));
    index_hi--;
     }
    result = BinaryVector::CreateFromRightAlignedBuffer(ReverseSliceBuffer,originalSlice.BitsCount());
   }
  
  
  return result ;
}

}
//
//  End of: BinaryVector::ReverseSlice
//---------------------------------------------------------------------------

//! Returns a slice from BinaryVector
//!
//! @note   This call is not valid if it defines a slice that exceed the actual
//!         bits count
//!
//! @param range          Ranges of bits to returns
//!
//! @return A new BinaryVector containing a copy of defined slice
BinaryVector BinaryVector::Slice (IndexedRange range) const
{
  return Slice(range.left, range.Width());
}
//
//  End of: BinaryVector::Slice
//---------------------------------------------------------------------------


//! Toggle specified bit
//!
//! @param bitOffset Zero based bit offset (from left)
//!
void BinaryVector::ToggleBit (uint32_t bitOffset)
{
  CHECK_PARAMETER_LT(bitOffset, m_usedBits, "Out of range bit id: "s + to_string(bitOffset));

  auto byteOffset      = bitOffset / 8;
  auto bitOffsetInByte = bitOffset % 8;

  m_data[byteOffset] ^= BIT_MASK_8[bitOffsetInByte];
}
//
//  End of: BinaryVector::ToggleBit
//---------------------------------------------------------------------------


//! Toggles (flips) every bits of the vector
//!
//! @note Unused bits remains forced to zero
//!
//! @return Same vector with all the used bits toggled
BinaryVector& BinaryVector::ToggleBits()
{
  for (auto& byte : m_data)
  {
    byte = ~byte;
  }

  MaskLastByte();

  return *this;
}
//
//  End of: BinaryVector::ToggleBits
//---------------------------------------------------------------------------



//! Truncates leading bits, providing they are all zeroes
//!
//! @param numberOfBitsToRemove   Number of leading zeroes to remove
//!
//! @return This BinaryVector or throw runtime_error if number of leading zeroes or total
//!         bits count is less than requested number of bits to remove
BinaryVector& BinaryVector::TruncateLeadingZeroes (uint32_t numberOfBitsToRemove)
{
  CHECK_PARAMETER_LTE(numberOfBitsToRemove, m_usedBits, "Cannot remove "s
                                                        .append(std::to_string(numberOfBitsToRemove))
                                                        .append(" bits when vector has only ")
                                                        .append(std::to_string(m_usedBits))
                                                        .append(" bits.\nCurrent value is: ")
                                                        .append(DataAsMixString()));

  CHECK_PARAMETER_LTE(numberOfBitsToRemove, LeadingZeroesCount(), "Cannot remove "s
                                                                  .append(std::to_string(numberOfBitsToRemove)).append(" leading zeroes from vector that has only ")
                                                                  .append(std::to_string(LeadingZeroesCount())).append(" leading zeroes.\nCurrent value is: ")
                                                                  .append(DataAsMixString()));

  auto remainingCount = m_usedBits - numberOfBitsToRemove;

  auto tmp = Slice(numberOfBitsToRemove, remainingCount);

  *this = std::move(tmp);
  return *this;
}
//
//  End of: BinaryVector::TruncateLeadingZeroes
//---------------------------------------------------------------------------


//===========================================================================
// End of BinaryVector.cpp
//===========================================================================
