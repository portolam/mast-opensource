//===========================================================================
//                           SystemModelNodes.hpp
//===========================================================================
// Copyright (C) 2016 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file SystemModelNodes.hpp
//!
//! Includes all header related to system model nodes
//!
//===========================================================================


#ifndef SYSTEMMODELNODES_H__AE0093C8_7CE7_463E_49B4_F6D79633F6FD__INCLUDED_
  #define SYSTEMMODELNODES_H__AE0093C8_7CE7_463E_49B4_F6D79633F6FD__INCLUDED_

#include "AccessInterface.hpp"
#include "AccessInterfaceTranslator.hpp"
#include "Register.hpp"
#include "Chain.hpp"
#include "Linker.hpp"
#include "Streamer.hpp"
//+#include "AiTranslator.hpp"

#endif  // not defined SYSTEMMODELNODES_H__AE0093C8_7CE7_463E_49B4_F6D79633F6FD__INCLUDED_
//===========================================================================
// End of SystemModelNodes.hpp
//===========================================================================



