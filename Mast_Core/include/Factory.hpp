//===========================================================================
//                           Factory.hpp
//===========================================================================
// Copyright (C) 2017 G-INP/Tima. All rights reserved.
//
// Project : Mast
//
//! @file Factory.hpp
//!
//! Declares Factory class
//!
//===========================================================================


#ifndef FACTORY_H__D3D0FAFB_AFA1_4AFE_F3AA_3AA69ECD884__INCLUDED_
  #define FACTORY_H__D3D0FAFB_AFA1_4AFE_F3AA_3AA69ECD884__INCLUDED_

#include <functional>
#include <memory>
#include <map>
#include <string>
#include <experimental/string_view>

#include "Mast_Core_export.hpp"

namespace mast
{
//! Defines common implementation for factories using register creation methods
//!
//! @note Creation methods must have been registered before calling Create (otherwise a nullptr is return)
//!       Concrete class are requested to implement InitializeWithDefaults (the common way to initialize default
//!       creation methods)
//!
template<typename BuildType,
         typename return_t  = std::unique_ptr<BuildType>,
         typename Creator_t = std::function<return_t(const std::string& parameters)>>
class MAST_CORE_EXPORT Factory
{
  // ---------------- Public  Methods
  //
  public:
  virtual ~Factory() = default;
  Factory()  = default;
  Factory(const Factory&) = delete;
  Factory& operator=(const Factory&) = delete;

//+  using Creator_t = std::function<std::unique_ptr<BuildType>(const std::string& parameters)>;

  //! Fills up with default creation methods
  //! @note Default creation methods a those that must not be registered explicitly
  //!       but provided by the concrete factory
  //!
  virtual void InitializeWithDefaults() = 0;

  //! Returns the number of factories currently registered (associated with a name)
  //!
  //! @note Mainly intended for test/check purpose
  size_t RegisteredCreatorsCount() const { return m_creators.size(); }

  //! Register an instance creation method
  //!
  //! @note If a factory already exists with the same factory name, it is replaced with the new one
  //!
  //! @param creatorId  Name associated with the creation function (typically named after the actual type to create)
  //! @param creator    Function that can create an instance of type associated with the concrete factory
  //!
  void RegisterCreator(const std::string& creatorId, Creator_t creator) { m_creators[creatorId] = creator; }

  //! Removes any, registered, factory
  //!
  void Clear() { m_creators.clear(); }


  //! Returns creation function associated with identifier
  //!
  //! @param creatorId    A name that identifies registered creation method
  //!
  bool HasCreator(const std::string& creatorId)
  {
    auto pos = m_creators.find(creatorId);
    return pos != m_creators.end();
  }
  
  //! Returns list of registerd creators
  std::string get_RegistredCreators()
   {
    std::string retvalue;
     for (auto itr : m_creators)
       retvalue.append(itr.first+" ");
   
    return retvalue;
   }


  //! Creates a BuildType instance using registered creation method and optional parameters
  //!
  //! @param creatorId    A name that identifies registered creation method
  //! @param parameters   String of (optional) parameters
  //!
  virtual return_t Create(const std::string& creatorId, const std::string& parameters = "") const = 0;

  // ---------------- Protected Methods
  //
  protected:

  //! Implements creation of actual instance.
  //!
  //! @note This is a default implementation provided for concrete factories to implement Create method
  //!
  //! @param creatorId    Creation method identifier
  //! @param parameters   Parameters to pass to creation method
  //!
  //!
  //! @return Created instance or nullptr when no creation method has been register with given name
  template<typename ... Args>
  return_t CreateImpl(const std::string& creatorId, Args&... args) const
  {
    return_t instance;

    auto pos = m_creators.find(creatorId);
    if (pos != m_creators.end())
    {
      instance = pos->second(args...);
    }

    return instance;
  }

  // ---------------- Private  Fields
  //
  private:
  std::map<std::string, Creator_t>  m_creators;
};
//
//  End of Factory class declaration
//---------------------------------------------------------------------------
} // End of namespace mast




#endif  // not defined FACTORY_H__D3D0FAFB_AFA1_4AFE_F3AA_3AA69ECD884__INCLUDED_

//===========================================================================
// End of Factory.hpp
//===========================================================================
